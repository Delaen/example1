<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%some_gds_provider_order}}`.
 */
class m191202_081343_create_some_gds_provider_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%some_gds_provider_order}}', [
            'id' => 'SERIAL',
            'user_id' => $this->integer()->notNull(),
            'order_data' => $this->json()->notNull(),
            'status' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY(user_id) REFERENCES {{%user}}(id) ON UPDATE CASCADE ON DELETE CASCADE',
            'PRIMARY KEY(id)'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%some_gds_provider_order}}');
    }
}
