<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%some_gds_provider_ticket_returns}}`.
 */
class m191204_040749_create_some_gds_provider_ticket_returns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%some_gds_provider_ticket_returns}}', [
            'id' => 'SERIAL',
            'order_id' => $this->integer()->notNull(),
            'ticket_number' => $this->string()->notNull(),
            'return_order_id' => $this->string()->notNull(),
            'return_order' => $this->json()->notNull(),
            'status' => $this->string()->notNull(),
            'bank_transaction' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY(order_id) REFERENCES {{%some_gds_provider_order}}(id) ON UPDATE CASCADE ON DELETE CASCADE',
            'PRIMARY KEY(id)'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%some_gds_provider_ticket_returns}}');
    }
}
