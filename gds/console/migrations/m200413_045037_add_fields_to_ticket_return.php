<?php

use yii\db\Migration;

/**
 * Class m200413_045037_add_fields_to_ticket_return
 */
class m200413_045037_add_fields_to_ticket_return extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%some_gds_provider_ticket_returns}}', 'reason', $this->string());
        $this->addColumn('{{%some_gds_provider_ticket_returns}}', 'accepted', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%some_gds_provider_ticket_returns}}', 'reason');
        $this->dropColumn('{{%some_gds_provider_ticket_returns}}', 'accepted');
    }
}
