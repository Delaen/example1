<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%some_gds_provider_cache}}`.
 */
class m191128_030848_create_some_gds_provider_cache_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%some_gds_provider_cache}}', [
            'hash' => $this->string(),
            'data' => $this->text()->notNull(),
            'expire_at' => $this->integer()->notNull(),
            'PRIMARY KEY (hash)'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%some_gds_provider_cache}}');
    }
}
