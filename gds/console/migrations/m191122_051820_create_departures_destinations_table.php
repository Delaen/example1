<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%departures_destinations}}`.
 */
class m191122_051820_create_departures_destinations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%some_gds_provider_departures_destinations}}', [
            'id_departure' => 'UUID',
            'id_destination' => 'UUID',
            'PRIMARY KEY(id_departure, id_destination)',
            'FOREIGN KEY (id_departure) REFERENCES some_gds_provider_bus_stops(id) ON UPDATE CASCADE ON DELETE CASCADE',
            'FOREIGN KEY (id_destination) REFERENCES some_gds_provider_bus_stops(id) ON UPDATE CASCADE ON DELETE CASCADE',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%some_gds_provider_departures_destinations}}');
    }
}
