<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bus_stops}}`.
 */
class m191030_085643_create_bus_stops_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%some_gds_provider_bus_stops}}', [
            'id' => 'UUID PRIMARY KEY',
            'gds_id' => $this->string(),
            'name' => $this->string(),
            'country' => $this->string(),
            'region' => $this->string(),
            'district' => $this->string(),
            'automated' => $this->boolean(),
            'has_destinations' => $this->boolean(),
            'locality' => $this->string(),
            'coordinates' => 'GEOGRAPHY(Point, 4326)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%some_gds_provider_bus_stops}}');
    }
}
