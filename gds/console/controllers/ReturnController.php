<?php
namespace app\modules\gds\console\controllers;

use app\modules\gds\components\someGdsProvider\ReturnInspector;
use yii\console\Controller;

class ReturnController extends Controller
{
    public function actionCheck()
    {
        $inspector = ReturnInspector::getInstance();
        $inspector->check();

        echo 'Checked ' . PHP_EOL;
    }
}