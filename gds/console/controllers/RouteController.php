<?php
namespace app\modules\gds\console\controllers;

use app\modules\gds\components\someGdsProvider\SomeGdsProviderCache;
use yii\console\Controller;

class RouteController extends Controller
{
    public function actionUpdateSomeGdsProvider()
    {
        $service = SomeGdsProviderCache::getInstance();
        echo $service->updateRoutes() ? 'Routes successfully updated ' . PHP_EOL : 'Routes not updated' . PHP_EOL;
    }
}