<?php

namespace app\modules\gds\controllers;

use app\modules\gds\components\someGdsProvider\SomeGdsProviderService;
use app\modules\gds\models\someGdsProvider\activeRecords\BusStop;
use app\modules\gds\models\someGdsProvider\activeRecords\Order as OrderAR;
use app\modules\gds\models\someGdsProvider\ChequeSettings;
use app\modules\gds\models\someGdsProvider\Constant;
use app\modules\gds\helpers\SomeGdsProviderExtractor;
use app\modules\gds\models\someGdsProvider\PaymentItem;
use app\modules\gds\models\someGdsProvider\PersonalData;
use app\modules\gds\components\someGdsProvider\SaleSession;
use app\modules\gds\models\someGdsProvider\TicketSeat;
use app\modules\gds\models\someGdsProvider\ValueVariant;
use Yii;
use yii\db\Query;
use yii\web\Controller;

class MainController extends Controller
{
    public function actionGetOrders(string $gds)
    {
        $result = [];
        $ordersAR = OrderAR::find()->where(['user_id' => Yii::$app->user->getId(), 'gds' => $gds])->indexBy('id')->all();

        foreach ($ordersAR as $id => $orderAR) {
            $result[$id] = $orderAR->order_data->toArray();
        }

        return $result;
    }

}
