<?php

namespace app\modules\gds\controllers;

use app\helpers\Notifier;
use app\helpers\PaymentHelper;
use app\modules\gds\helpers\PaymentManager;
use app\modules\gds\models\someGdsProvider\activeRecords\Order as OrderAR;
use app\modules\gds\components\someGdsProvider\SomeGdsProviderCache;
use app\modules\gds\components\someGdsProvider\SomeGdsProviderService;
use app\modules\gds\components\someGdsProvider\SaleSession;
use app\modules\gds\models\someGdsProvider\activeRecords\TicketReturn;
use app\modules\gds\models\someGdsProvider\Constant;
use app\modules\gds\models\someGdsProvider\TicketSeat;
use app\modules\gds\models\someGdsProvider\Trip;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use Yii;
use yii\web\HttpException;
use yii\web\Response;

class SomeGdsProviderController extends Controller
{
    /**
     * @var SomeGdsProviderService
     */
    public $service;
    /**
     * @var SomeGdsProviderCache
     */
    public $cache;
    /**
     * @var array
     */
    public $requestParams = [];

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/xml'] = Response::FORMAT_JSON;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['start-sale', 'add-tickets', 'add-ticket', 'get-privileges', 'set-privilege', 'reserve-order',
                        'delete-ticket', 'add-ticket-return', 'payment', 'accept-returns', 'delete-tickets-from-session', 'set-ticket-data'],
                    'roles' => ['@'],
                ],
                [
                    'allow' => true,
                    'actions' => ['get-departures', 'get-localities', 'get-bus-stops', 'get-destinations', 'default-departure',
                        'get-trips', 'get-trip-route'],
                    'roles' => ['@', '?'],
                ],

            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'get-destinations' => ['get'],
                'get-departures' => ['get'],
                'get-trip-route' => ['get'],
                'get-localities' => ['get'],
                'start-sale' => ['post'],
                'add-tickets' => ['post'],
                'add-ticket' => ['post'],
                'get-privileges' => ['get'],
                'set-privilege' => ['post'],
                'delete-ticket' => ['post'],
                'delete-tickets-from-session' => ['post'],
                'add-ticket-return' => ['post'],
                'reserve-order' => ['post'],
                'accept-returns' => ['post'],
                'payment' => ['post'],
            ],
        ];
        return $behaviors;
    }

    /**
     * @param $action
     * @return bool
     * @throws \SoapFault
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->service = SomeGdsProviderService::getInstance();
        $this->cache = SomeGdsProviderCache::getInstance();
        $this->requestParams = array_merge(Yii::$app->request->getQueryParams(), Yii::$app->request->getBodyParams());

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($result === null || $result === false) {
            Yii::$app->response->statusCode = 400;
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Updates information about bus stops and their destinations
     * @return bool
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdateRoutes()
    {
        return $this->cache->updateRoutes();
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionGetDepartures(): array
    {
        return $this->cache->getDepartures();
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionGetLocalities()
    {
        return $this->cache->getLocalities();
    }

    /**
     * @return array|null
     * @throws \yii\db\Exception
     */
    public function actionGetBusStops()
    {
        if (array_key_exists('locality', $this->requestParams)) {
            return $this->cache->getBusStopsByLocality($this->requestParams['locality']);
        }

        return null;
    }

    /**
     * @return array|null
     * @throws \yii\db\Exception
     */
    public function actionGetDestinations(): ?array
    {
        if (array_key_exists('departureId', $this->requestParams)) {
            return $this->cache->getDestinations($this->requestParams['departureId']);
        }

        return [];
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionDefaultDeparture()
    {
        return $this->cache->getDefaultDeparture();
    }

    /**
     * @return array
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function actionGetTrips(): ?array
    {
        if (empty(array_diff(['departureId', 'destinationId', 'tripDate', 'personsNumber'], array_keys($this->requestParams)))) {
            $filterParams['personsNumber'] = $this->requestParams['personsNumber'];
            array_key_exists('type', $this->requestParams) && $filterParams['comment'] = $this->requestParams['type'];

            $trips = $this->cache->getTrips($this->requestParams['departureId'], $this->requestParams['destinationId'],
                $this->requestParams['tripDate'], $filterParams);

            return ($trips instanceof Trip) ? [$trips->toArrayForTripList()] : Trip::toArrayForTripListMultiple($trips);
        }

        return [];
    }

    /**
     * @return array|null
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function actionGetTripRoute()
    {
        if (empty(array_diff(['tripId', 'departureId'], array_keys($this->requestParams)))) {

            $trip = $this->cache->getTrip($this->requestParams['tripId'], $this->requestParams['departureId']);
            return ($trip instanceof Trip) ? $trip->toArrayRouteItems() : null;
        }

        return null;
    }

    /**
     * @return array|null
     * @throws HttpException
     * @throws \SoapFault
     */
    public function actionStartSale()
    {
        if (empty(array_diff(['tripId', 'departureId', 'destinationId'], array_keys($this->requestParams)))) {
            $saleSession = SomeGdsProviderCache::getInstance()->startSaleSession($this->requestParams['tripId'], $this->requestParams['departureId'], $this->requestParams['destinationId']);
            return ($saleSession instanceof SaleSession) ? $saleSession->getOrder()->toArray() : null;
        }

        return null;
    }

    /**
     * @return array|null
     * @throws HttpException
     * @throws \ReflectionException
     */
    public function actionAddTickets()
    {
        if (array_key_exists('tickets', $this->requestParams) && is_array($this->requestParams['tickets'])) {
            $saleSession = SaleSession::getInstance();
            $saleSession->restore();
            $tickets = [];

            foreach ($this->requestParams['tickets'] as $ticket) {
                if (!is_array($ticket) || !empty(array_diff(['type', 'seatNumber'], array_keys($ticket))) || !in_array($ticket['type'], Constant::fareTypes())) {
                    return null;
                }

                $ticketSeat = TicketSeat::createObject();
                $ticketSeat->FareName = Constant::fareTypes(true)[$ticket['type']];
                $ticketSeat->SeatNum = $ticket['type'] !== Constant::FARE_TYPE_BAGGAGE ? $ticket['seatNumber'] : 0;
                isset($ticket['parent']) && $ticketSeat->ParentTicketSeatNum = $ticket['parent'];

                $tickets[] = $ticketSeat;
            }

            return $saleSession->addTickets($tickets) ? $saleSession->getOrder()->toArray() : null;
        }

        return null;
    }

    /**
     * @return array|null
     * @throws HttpException
     * @throws \ReflectionException
     */
    public function actionAddTicket()
    {
        $saleSession = SaleSession::getInstance();
        $saleSession->restore();

        $ticketSeat = TicketSeat::createObject();
        return $saleSession->addTickets([$ticketSeat]) ? $saleSession->getOrder()->toArray() : null;
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionGetPrivileges()
    {
        $saleSession = SaleSession::getInstance();
        $saleSession->restore();

        $result = [];
        $privileges = $saleSession->getPrivileges();
        foreach ($privileges as $ticketNumber => $privilegesArray) {
            $result[$ticketNumber] = array_map(function ($element) {
                return $element->Name;
            }, $privilegesArray);
        }

        return $result;
    }

    /**
     * @return array|null
     * @throws HttpException
     */
    public function actionSetPrivilege()
    {
        if (empty(array_diff(['ticketNumber', 'privilege'], array_keys($this->requestParams)))) {
            $saleSession = SaleSession::getInstance();
            $saleSession->restore();

            return $saleSession->setPrivilege($this->requestParams['ticketNumber'], $this->requestParams['privilege']) ? $saleSession->getOrder()->toArray() : null;
        }

        return null;
    }

    /**
     * @return array|null
     * @throws HttpException
     * @throws \ReflectionException
     * @throws \yii\db\Exception
     */
    public function actionSetTicketData()
    {
        if (array_key_exists('tickets', $this->requestParams) && is_array($this->requestParams['tickets'])) {
            $saleSession = SaleSession::getInstance();
            $saleSession->restore();

            $transaction = Yii::$app->db->beginTransaction();
            if (!$saleSession->setTicketsData($this->requestParams['tickets'])) {
                $transaction->rollBack();
                return null;
            }

            $transaction->commit();
            $order = $saleSession->getOrder()->toArray();
            return $order;
        }

        return null;
    }

    /**
     * @return string|null
     * @throws HttpException
     * @throws \ReflectionException
     * @throws \yii\db\Exception
     */
    public function actionReserveOrder()
    {
        if (array_key_exists('tickets', $this->requestParams) && is_array($this->requestParams['tickets'])) {
            $saleSession = SaleSession::getInstance();
            $saleSession->restore();

            $transaction = Yii::$app->db->beginTransaction();
            if (!$saleSession->setTicketsData($this->requestParams['tickets']) || !$saleSession->reserveOrder()) {
                $transaction->rollBack();
                return null;
            }

            $transaction->commit();
            $saleSession->destroy();
            $order = $saleSession->getOrderAR();

            return PaymentHelper::createPaymentOrder(
                $order->id,
                Yii::$app->user->identity->id,
                (integer)((float)$order->order_data->getTotalCost() * 100),
                $order->getOrderBundle()
            );
        }

        return null;
    }

    /**
     * @return bool
     * @throws HttpException
     * @throws \ReflectionException
     */
    public function actionPayment()
    {
        if (empty(array_diff(['orderId', 'amount'], array_keys($this->requestParams)))) {
            return PaymentManager::payOrder($this->requestParams['orderId'], $this->requestParams['amount']);
        }

        return false;
    }

    /**
     * @return bool|null
     * @throws HttpException
     * @throws \ReflectionException
     * @throws \SoapFault
     */
    public function actionDeleteTicket()
    {
        if (empty(array_diff(['orderId', 'ticketId'], array_keys($this->requestParams))) &&
            !empty($orderAR = OrderAR::findOne($this->requestParams['orderId'])) &&
            !(empty($ticket = $orderAR->order_data->getTicket($this->requestParams['ticketId'])))) {

            $ticketSeat = TicketSeat::createObject();
            $ticketSeat->SeatNum = $ticket->SeatNum;

            $result = $this->service->deleteTickets($orderAR->order_data->Number, [$ticketSeat]);
            if ($result instanceof Order) {
                $orderAR->order_data = $result;
                $orderAR->save();
            }

            return (bool)$result;
        }

        return null;
    }

    /**
     * @return array|bool|null
     * @throws HttpException
     * @throws \ReflectionException
     */
    public function actionDeleteTicketsFromSession()
    {
        if (array_key_exists('tickets', $this->requestParams) && is_array($this->requestParams['tickets'])) {
            $saleSession = SaleSession::getInstance();
            $saleSession->restore();
            $numbers = [];

            foreach ($this->requestParams['tickets'] as $number) {
                $number && $numbers[] = $number;
            }

            return $saleSession->deleteTickets($numbers) ? $saleSession->getOrder()->toArray() : false;
        }

        return null;
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function actionAddTicketReturn()
    {
        if (empty(array_diff(['ticketNumber', 'orderId', 'reason'], array_keys($this->requestParams)))) {
            $result = PaymentManager::returnTicket(
                $this->requestParams['orderId'],
                $this->requestParams['ticketNumber'],
                $this->requestParams['reason']
            );

            return $result->getTicketReturns()[0]->toArray();
        }

        return null;
    }

    /**
     * @return bool|null
     * @throws \yii\db\Exception
     */
    public function actionAcceptReturns(): ?bool
    {
        if (empty(array_diff(['tickets'], array_keys($this->requestParams))) &&
            is_array($this->requestParams['tickets'])) {

            $transaction = Yii::$app->db->beginTransaction();
            $tickets = TicketReturn::findAll(['ticket_number' => $this->requestParams['tickets']]);

            if (count($tickets) !== count($this->requestParams['tickets'])) {
                return false;
            }

            foreach ($tickets as $ticket) {
                $ticket->accepted = true;
                if (!$ticket->save()) {
                    return false;
                }
            }

            $transaction->commit();
            Notifier::returnOrder($tickets[0]->order->getRouteString(), $this->requestParams['tickets']);
            return true;
        }

        return null;
    }

    /**
     * @return bool|null
     * @throws HttpException
     * @throws \SoapFault
     * @throws \yii\db\Exception
     */
    /*public function actionAddTicketReturn()
    {
        if (empty(array_diff(['ticketId', 'orderId'], array_keys($this->requestParams)))) {
            return PaymentManager::returnTicket($this->requestParams['orderId'], $this->requestParams['ticketId']);
        }

        return null;
    }*/

    /*
      public function actionAddTicketReturn()
      {
          if (empty(array_diff(['tickets', 'orderId', 'reason'], array_keys($this->requestParams))) &&
              is_array($this->requestParams['tickets'])) {

              $result = [];
              foreach ($this->requestParams['tickets'] as $ticketId) {
                  try {
                      $returnOrder = PaymentManager::returnTicket($this->requestParams['orderId'], $ticketId);
                      $result[$ticketId] = $returnOrder->getTicketReturns()[0]->toArray();
                  } catch (\Exception $e) {
                      $result[$ticketId] = [];
                  }
              }

              return $result ?: null;
          }

          return null;
      }
  */
}
