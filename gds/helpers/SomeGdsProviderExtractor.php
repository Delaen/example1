<?php
namespace app\modules\gds\helpers;

use app\modules\gds\models\someGdsProvider\GdsObject;

abstract class SomeGdsProviderExtractor
{
    /**
     * @param $object
     * @param $className
     * @return GdsObject|null
     */
    static function extractObject($object, $className): ?GdsObject
    {
        if($object instanceof \stdClass) {
            return !empty(get_object_vars($object)) ? new $className($object) : null;
        }

        return null;
    }

    /**
     * @param $objects
     * @param $className
     * @return GdsObject|array|null
     */
    static function extractObjects($objects, $className)
    {
        if (is_array($objects)) {
            $result = [];
            foreach ($objects as $object) {
                ($object instanceof \stdClass) && $result[] = self::extractObject($object, $className);
            }
            return $result;

        }elseif($objects instanceof \stdClass) {
            return self::extractObject($objects, $className);
        }

        return null;
    }

    /**
     * @param GdsObject $object
     * @return \stdClass
     */
    static function restoreObject(GdsObject $object): \stdClass
    {
        $stdObject = new \stdClass();
        foreach ($object as $property => $value) {
            if(is_array($value)) {
                $stdObject->$property = self::restoreArray($value);
            }elseif (is_object($value)) {
                $stdObject->$property = self::restoreObject($value);
            }else {
                $property !== 'vendor' && $stdObject->$property = $value;
            }
        }

        return $stdObject;
    }

    /**
     * @param $objects
     * @return array|\stdClass|null
     */
    static function restoreObjects($objects)
    {
        if (is_array($objects)) {
            $result = [];
            foreach ($objects as $object) {
                ($object instanceof GdsObject) && $result[] = self::restoreObject($object);
            }

            return $result;
        }elseif ($objects instanceof GdsObject) {
            return self::restoreObject($objects);
        }

        return null;
    }

    /**
     * @param array $propertyValues
     * @return array
     */
    static function restoreArray(array $propertyValues): array
    {
        $resultArray = [];
        foreach ($propertyValues as $key => $value){
            if (is_array($value)){
                $resultArray[$key] = self::restoreArray($value);
            }elseif (is_object($value)) {
                $resultArray[$key] = self::restoreObject($value);
            }else {
                $resultArray[$key] = $value;
            }
        }

        return $resultArray;
    }
}