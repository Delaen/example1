<?php
namespace app\modules\gds\helpers;

use app\modules\gds\models\someGdsProvider\activeRecords\BusStop;
use aracoool\uuid\Uuid;
use yii\web\HttpException;

/**
 * Class SomeGdsProviderHelper
 * @package app\modules\gds\helpers
 */
abstract class SomeGdsProviderHelper
{
    /**
     * @param string $gdsId
     * @return string|null
     * @throws HttpException
     */
    static function actualBusStopId(string $gdsId): ?string
    {
        if (!Uuid::isValid($gdsId)) {
            throw new HttpException(400, 'Формат UUID неверен');
        }

        $result = BusStop::find()->where(['gds_id' => $gdsId])->limit(1)->column();
        return !empty($result) ? $result[0] : null;
    }
}