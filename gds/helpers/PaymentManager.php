<?php

namespace app\modules\gds\helpers;

use app\modules\gds\components\someGdsProvider\SomeGdsProviderService;
use app\modules\gds\components\someGdsProvider\jobs\TicketReturnJob;
use app\modules\gds\models\someGdsProvider\activeRecords\Order as OrderAR;
use app\modules\gds\models\someGdsProvider\activeRecords\TicketReturn;
use app\modules\gds\models\someGdsProvider\ChequeSettings;
use app\modules\gds\models\someGdsProvider\Constant;
use app\modules\gds\models\someGdsProvider\Order;
use app\modules\gds\models\someGdsProvider\PaymentItem;
use app\modules\gds\models\someGdsProvider\ReturnOrder;
use yii\web\HttpException;
use Yii;

abstract class PaymentManager
{
    /**
     * @param string $orderId
     * @param float $amount
     * @param bool $withUserCheck
     * @return bool
     * @throws HttpException
     * @throws \ReflectionException
     * @throws \SoapFault
     * @throws \yii\db\Exception
     */
    public static function payOrder(string $orderId, float $amount, bool $withUserCheck = false): bool
    {
        Yii::info("Запрос на оплату, заказ №$orderId, сумма - $amount" , 'someGdsProvider.transactions.payment');

        try {
            if (!empty($orderAR = OrderAR::findOne($orderId))) {
                if ($withUserCheck && ($orderAR->user_id != Yii::$app->user->getId())) {
                    throw new HttpException(400, 'Нельзя оплатить чужой заказ');
                }

                if ($orderAR->status == Constant::ORDER_STATUS_PAID) {
                    throw new HttpException(400, 'Заказ уже оплачен');
                }

                if ($orderAR->order_data->getTotalCost() != (float)$amount) {
                    throw new HttpException(400, 'Сумма оплаты неверна');
                }

                $transaction = Yii::$app->db->beginTransaction();
                $orderAR->status = Constant::ORDER_STATUS_PAID;

                if ($orderAR->save()) {
                    $service = SomeGdsProviderService::getInstance();
                    $paymentItem = PaymentItem::createObject();
                    $paymentItem->Amount = $amount;
                    $paymentItem->PaymentType = Constant::PAYMENT_TYPE_PAYMENT_CARD;

                    $cheque = ChequeSettings::createObject();
                    $cheque->ChequeWidth = Constant::CHEQUE_WIDTH;

                    Yii::info('Запрос на оплату в авибус', 'someGdsProvider.transactions.payment');
                    $result = $service->payment($orderAR->order_data->Number, [$paymentItem], $cheque);

                    if (!($result instanceof Order)) {
                        throw new HttpException(400, 'Оплата не прошла');
                    }

                    $orderAR->order_data = $result;
                    $orderAR->save();
                    $transaction->commit();
                    Yii::info("Заказ оплачен, заказ №$orderId, сумма - $amount", 'someGdsProvider.transactions.payment');
                    return true;
                }
            }
        }catch (\Exception $e) {
            Yii::error($e->getMessage() . ", заказ №$orderId, сумма - $amount");
            throw $e;
        }

        Yii::error("Зака №$orderId не найден", 'someGdsProvider.transactions.payment');
        return false;
    }

    /**
     * @param string $orderId
     * @param string $ticketNumber
     * @param string $reason
     * @param bool $withUserCheck
     * @return ReturnOrder|bool
     * @throws \Exception
     */
    public static function returnTicket(string $orderId, string $ticketNumber, string $reason = '', bool $withUserCheck = false)
    {
        try {
            if (!empty($orderAR = OrderAR::find()->with('ticketReturns')->where(['id' => $orderId])->one()) &&
                !empty($ticket = $orderAR->order_data->getTicket($ticketNumber))) {

                if ($orderAR->status === Constant::TICKET_RETURN_STATUS_NOT_PAID) {
                    throw new HttpException(400, 'Заказ не оплачен');
                }

                $transaction = Yii::$app->db->beginTransaction();
                if (in_array($ticketNumber, $orderAR->getReturnTicketsNumbers())) {
                    $returnTicket = TicketReturn::findOne(['order_id' => $orderId, 'ticket_number' => $ticketNumber]);
                    if ($returnTicket->accepted) {
                        throw new HttpException(400, 'Возврат на указанный билет уже создавался');
                    }

                    $returnTicket->delete();
                }

                if ($withUserCheck && (Yii::$app->user->getId() != $orderAR->user_id)) {
                    throw new HttpException(400, 'Это не ваш заказ!');
                }

                $service = SomeGdsProviderService::getInstance();
                $result = $service->addTicketReturn($ticket->Number, $ticket->Departure->Id);
                $returnTicket = new TicketReturn([
                    'order_id' => $orderAR->id,
                    'ticket_number' => $ticketNumber,
                    'return_order_id' => $result->Number,
                    'return_order' => $result,
                    'reason' => $reason
                ]);

                Yii::info("Запрос на создание заказа на возврат, заказ №$orderId, билет №$ticketNumber");
                if (!$returnTicket->save() || !($result instanceof ReturnOrder)) {
                    throw new HttpException(400, 'Ошибка создания заявки на возврат');
                }

                //self::payReturn($returnTicket->id);
                $transaction->commit();

                return $result;
            }
        }catch (\Exception $e) {
            Yii::error($e->getMessage() . ", заказ №$orderId, билет №$ticketNumber");
            throw $e;
        }

        Yii::error("Заказ или билет не найден, заказ №$orderId, билет №$ticketNumber");
        return false;
    }

    /**
     * @param string $ticketReturnId
     */
    private static function payReturn(string $ticketReturnId)
    {
        Yii::$app->someGdsProviderQueue->push(new TicketReturnJob([
            'ticketReturnId' => $ticketReturnId
        ]));
    }
}