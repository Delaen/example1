<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class PersonalData extends GdsObject
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Caption;
    /**
     * @var null|boolean
     */
    public $Mandatory;
    /**
     * @var null|boolean
     */
    public $PersonIdentifier;
    /**
     * @var null|string
     */
    public $Type;
    /**
     * @var null|ValueVariant|ValueVariant[]
     */
    public $ValueVariants;
    /**
     * @var null|string
     */
    public $InputMask;
    /**
     * @var null|string
     */
    public $Value;
    /**
     * @var null|string
     */
    public $ValueKind;
    /**
     * @var null|ValueVariant|ValueVariant[]
     */
    public $DefaultValueVariant;
    /**
     * @var null|boolean
     */
    public $DocumentIssueDateRequired;
    /**
     * @var null|boolean
     */
    public $DocumentIssueOrgRequired;
    /**
     * @var null|boolean
     */
    public $DocumentValidityDateRequired;
    /**
     * @var null|boolean
     */
    public $DocumentInceptionDateRequired;
    /**
     * @var null|boolean
     */
    public $DocumentIssuePlaceRequired;
    /**
     * @var null|string
     */
    public $Value1;
    /**
     * @var null|string
     */
    public $Value2;
    /**
     * @var null|string
     */
    public $Value3;
    /**
     * @var null|string
     */
    public $Value4;
    /**
     * @var null|string
     */
    public $Value5;

    /**
     * PersonalData constructor.
     * @param \stdClass $object
     */
    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'DefaultValueVariant') && $this->DefaultValueVariant = SomeGdsProviderExtractor::extractObject($object->DefaultValueVariant, ValueVariant::class);;

        if (property_exists($object, 'ValueVariants')) {
            $this->ValueVariants = SomeGdsProviderExtractor::extractObjects($object->ValueVariants, ValueVariant::class);
        }
    }

    /**
     * @param bool $withVariants
     * @return array
     */
    public function toArray($withVariants = false)
    {
        $result = [];
        $type = $this->getType();
        if ($type) {
            if ($type === 'document') {
                $result = ['type' => $type, 'docType' => $this->ValueKind, 'value' => $this->Value];
            }elseif ($type === 'privilegeDoc') {
                $result = ['type' => $type, 'docType' => $this->Name, 'value' => $this->Value];
            } else {
                $result = ['type' => $type, 'value' => $this->Value];
            }

            if ($withVariants) {
                $result['valueVariants'] = is_array($this->ValueVariants) ? ValueVariant::toArrayMultiple($this->ValueVariants) :
                    (($this->ValueVariants instanceof ValueVariant) ? [$this->ValueVariants->toArray()] : []);
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function checkData()
    {
        return $this->Name && $this->Value;
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        $type = null;

        if ($this->Name) {
            switch ($this->Name) {
                case 'Удостоверение':
                    $type = Constant::DOC_TYPE;
                    break;
                case 'ФИО':
                    $type = Constant::DOC_FIO;
                    break;
                case 'Дата рождения':
                    $type = Constant::DOC_BIRTHDAY;
                    break;
                case 'Пол':
                    $type = Constant::DOC_GENDER;
                    break;
                case 'Гражданство':
                    $type = Constant::DOC_NATIONALITY;
                    break;
                case 'Мобильный телефон':
                    $type = Constant::DOC_PHONE;
                    break;
                default:
                    $type = Constant::PRIVILEGE_DOC;
                    break;
            }

            return $type;
        }

        return null;
    }
}