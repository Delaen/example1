<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class TicketReturn extends GdsObject
{
    /**
     * @var null|string
     */
    public $Number;
    /**
     * @var null|Ticket
     */
    public $Ticket;
    /**
     * @var null|string
     */
    public $ReturnKind;
    /**
     * @var null|bool
     */
    public $NeedExplanation;
    /**
     * @var null|string
     */
    public $Explanation;
    /**
     * @var null|ReturnCalculationFee[]
     */
    public $Fees;
    /**
     * @var null|Cheque[]
     */
    public $Cheques;
    /**
     * @var null|string
     */
    public $ReturnKindDescription;
    /**
     * @var null|float
     */
    public $FareToReturn;
    /**
     * @var null|float
     */
    public $SumToReturn;
    /**
     * @var null|float
     */
    public $FaultDistance;
    /**
     * @var null|string
     */
    public $AvailableReturnKinds;
    /**
     * @var null|MarketingCampaign
     */
    public $MarketingCampaign;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);

        property_exists($object, 'Ticket') && $this->Ticket = SomeGdsProviderExtractor::extractObject($object->Ticket, Ticket::class);
        property_exists($object, 'MarketingCampaign') && $this->MarketingCampaign = SomeGdsProviderExtractor::extractObject($object->MarketingCampaign, MarketingCampaign::class);

        if (property_exists($object, 'Fees')) {
            $this->Fees = SomeGdsProviderExtractor::extractObjects($object->Fees, ReturnCalculationFee::class);
        }
        if (property_exists($object, 'Cheques')) {
            $this->Cheques = SomeGdsProviderExtractor::extractObjects($object->Cheques, Cheque::class);
        }
    }

    /**
     * @return int
     */
    public function getReturnTaxAmount(): int
    {
        $returnTax = 0;
        if (is_array($this->Fees)) {
            foreach ($this->Fees as $fee) {
                $returnTax =+ $fee->SumToReturn;
            }
        }elseif(is_object($this->Fees)) {
            $returnTax = $this->Fees->SumToReturn;
        }

        return $returnTax;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $sum = (float) $this->SumToReturn;
        $tax = $this->Ticket->getTaxAmount();
        $returnTax = $this->getReturnTaxAmount();

        return [
            'totalSum' => $sum,
            'tax' => $tax,
            'returnTax' => $returnTax,
            'totalTax' => $tax,
            'returnKind' => $this->ReturnKindDescription
        ];
    }
}