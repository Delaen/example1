<?php
namespace app\modules\gds\models\someGdsProvider;

abstract class Constant
{
    const CHEQUE_WIDTH = 48;

    //Trip Statuses
    const TRIP_STATUS_CANCELED = 'Canceled';
    const TRIP_STATUS_DEPARTED = 'Departed';
    const TRIP_STATUS_ARRIVED = 'Arrived';
    const TRIP_STATUS_WAITING = 'Waiting';
    const TRIP_STATUS_UNKNOWN = 'Unknown';

    static function tripStatuses($withLabels = false)
    {
        return $withLabels ? [
            self::TRIP_STATUS_CANCELED => 'Отменен',
            self::TRIP_STATUS_DEPARTED => 'Отправлен',
            self::TRIP_STATUS_ARRIVED => 'Приехал',
            self::TRIP_STATUS_WAITING => 'В ожидании',
            self::TRIP_STATUS_UNKNOWN => 'Неизвестно',
        ] : [
            self::TRIP_STATUS_CANCELED,
            self::TRIP_STATUS_DEPARTED,
            self::TRIP_STATUS_ARRIVED,
            self::TRIP_STATUS_WAITING,
            self::TRIP_STATUS_UNKNOWN,
        ];
    }

    //Seats Types
    const SEAT_TYPE_PASSENGER = 'Passenger';
    const SEAT_TYPE_PASSENGER_STAND = 'PassengerStand';
    const SEAT_TYPE_BAGGAGE = 'Baggage';
    const SEAT_TYPE_WITHOUT_SEATS = 'WithoutSeats';

    static function seatsStatuses($withLabels = false)
    {
        return $withLabels ? [
            self::SEAT_TYPE_PASSENGER => 'Пассажирский',
            self::SEAT_TYPE_PASSENGER_STAND => 'Пасажирский стоячий',
            self::SEAT_TYPE_BAGGAGE => 'Багажный',
            self::SEAT_TYPE_WITHOUT_SEATS => 'Без сидения',
        ] : [
            self::SEAT_TYPE_PASSENGER,
            self::SEAT_TYPE_PASSENGER_STAND,
            self::SEAT_TYPE_BAGGAGE,
            self::SEAT_TYPE_WITHOUT_SEATS,
        ];
    }

    //BusSeat statuses
    const BUS_SEAT_FREE = 'Free';
    const BUS_SEAT_ON_MY_SALE = 'OnMySale';
    const BUS_SEAT_ON_SALE = 'OnSale';
    const BUS_SEAT_RESERVED = 'Reserved';
    const BUS_SEAT_SOLD = 'Sold';

    static function busSeatsStatuses($withLabels = false)
    {
        return $withLabels ? [
            self::BUS_SEAT_FREE => 'Свободно',
            self::BUS_SEAT_ON_MY_SALE => 'В процессе продажи (этот заказ)',
            self::BUS_SEAT_ON_SALE => 'В процессе продажи',
            self::BUS_SEAT_RESERVED => 'Забронировано',
            self::BUS_SEAT_SOLD => 'Продано',
        ] : [
            self::BUS_SEAT_FREE,
            self::BUS_SEAT_ON_MY_SALE,
            self::BUS_SEAT_ON_SALE,
            self::BUS_SEAT_RESERVED,
            self::BUS_SEAT_SOLD,
        ];
    }

    //Sales Statuses
    const SALE_STATUS_SUSPENDED = 'suspended';
    const SALE_STATUS_ON_SALE = 'OnSale';
    const SALE_STATUS_SALE_ON_BOARDING = 'SaleOnBoarding';

    static function saleStatuses($withLabels = false)
    {
        return $withLabels ? [
            self::SALE_STATUS_SUSPENDED => '',
            self::SALE_STATUS_ON_SALE => '',
            self::SALE_STATUS_SALE_ON_BOARDING => '',
        ] : [
            self::SALE_STATUS_SUSPENDED,
            self::SALE_STATUS_ON_SALE,
            self::SALE_STATUS_SALE_ON_BOARDING,
        ];
    }

    //PaymentTypes
    const PAYMENT_TYPE_CASH = 'Cash';
    const PAYMENT_TYPE_LOYALTY_CARD = 'LoyaltyCard';
    const PAYMENT_TYPE_PAYMENT_CARD = 'PaymentCard';
    const PAYMENT_TYPE_PAYMENT_ON_BOARD = 'PaymentOnTheBoarding';
    const PAYMENT_TYPE_DEBIT_PAYMENT = 'DebitPayment';
    const PAYMENT_TYPE_OTHER = 'Other';

    static function paymentTypes($withLabels = false)
    {
        return $withLabels ? [
            self::PAYMENT_TYPE_CASH => '',
            self::PAYMENT_TYPE_LOYALTY_CARD => '',
            self::PAYMENT_TYPE_PAYMENT_CARD => '',
            self::PAYMENT_TYPE_PAYMENT_ON_BOARD => '',
            self::PAYMENT_TYPE_DEBIT_PAYMENT => '',
            self::PAYMENT_TYPE_OTHER => '',
        ] : [
            self::PAYMENT_TYPE_CASH,
            self::PAYMENT_TYPE_LOYALTY_CARD,
            self::PAYMENT_TYPE_PAYMENT_CARD,
            self::PAYMENT_TYPE_PAYMENT_ON_BOARD,
            self::PAYMENT_TYPE_DEBIT_PAYMENT,
            self::PAYMENT_TYPE_OTHER,
        ];
    }

    //Fare types
    const FARE_TYPE_FULL = 'fare-full';
    const FARE_TYPE_CHILD = 'fare-child';
    const FARE_TYPE_BAGGAGE = 'fare-baggage';

    static function fareTypes($withDescription = false)
    {
        return $withDescription ? [
            self::FARE_TYPE_FULL => 'Пассажирский',
            self::FARE_TYPE_CHILD => 'Детский',
            self::FARE_TYPE_BAGGAGE => 'Багажный'
        ] : [
            self::FARE_TYPE_FULL, self::FARE_TYPE_CHILD, self::FARE_TYPE_BAGGAGE
        ];
    }

    const ORDER_STATUS_NOT_PAID = 'not-paid';
    const ORDER_STATUS_PAID = 'paid';

    static function orderStatuses($withDescription = false)
    {
        return $withDescription ?
            [
                self::ORDER_STATUS_NOT_PAID => 'Не оплачено',
                self::ORDER_STATUS_PAID => 'Оплачено'
            ]:
             [self::ORDER_STATUS_NOT_PAID, self::ORDER_STATUS_PAID];
    }

    const TICKET_RETURN_STATUS_NOT_PAID = 'not-paid';
    const TICKET_RETURN_STATUS_PAID = 'paid';

    static function ticketReturnStatuses($withDescription = false)
    {
        return $withDescription ?
            [
                self::TICKET_RETURN_STATUS_NOT_PAID => 'Не оплачено',
                self::TICKET_RETURN_STATUS_PAID => 'Оплачено'
            ]:
            [self::TICKET_RETURN_STATUS_NOT_PAID, self::TICKET_RETURN_STATUS_PAID];
    }

    //Personal Data variants
    const DOC_TYPE = 'document';
    const DOC_FIO = 'fio';
    const DOC_BIRTHDAY = 'birthday';
    const DOC_GENDER = 'gender';
    const DOC_NATIONALITY = 'nationality';
    const DOC_PHONE = 'phone';
    const PRIVILEGE_DOC = 'privilegeDoc';

    static function personalDataVariants()
    {
        return [
            self::DOC_TYPE,
            self::DOC_FIO,
            self::DOC_BIRTHDAY,
            self::DOC_GENDER,
            self::DOC_NATIONALITY,
            self::DOC_PHONE,
            self::PRIVILEGE_DOC
        ];
    }

    const RETURN_REASON_VOLUNTARY = 'voluntary';
    const RETURN_REASON_CARRIER = 'carrier';
    const RETURN_REASON_OTHER = 'other';

    static function ticketReturnReasons($withDescription = false)
    {
        return $withDescription ?
            [
                self::RETURN_REASON_VOLUNTARY => 'Добровольный возврат пассажиром',
                self::RETURN_REASON_CARRIER => 'Отмена рейса по причине перевозчика',
                self::RETURN_REASON_OTHER => 'Иная причина'
            ]:
            [
                self::RETURN_REASON_VOLUNTARY,
                self::RETURN_REASON_CARRIER,
                self::RETURN_REASON_OTHER,
            ];
    }
}