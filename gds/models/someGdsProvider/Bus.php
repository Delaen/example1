<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class Bus extends GdsObject
{
    /**
     * @var null|string
     */
    public $Id;
    /**
     * @var null|string
     */
    public $Model;
    /**
     * @var null|string
     */
    public $LicencePlate;
    /**
     * @var null|string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $SeatsClass;
    /**
     * @var null|int
     */
    public $SeatCapacity;
    /**
     * @var null|int
     */
    public $StandCapacity;
    /**
     * @var null|int
     */
    public $BaggageCapacity;
    /**
     * @var array
     */
    public $SeatsScheme = [];
    /**
     * @var null|string
     */
    public $GarageNum;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        if (property_exists($object, 'SeatsScheme')) {
            $this->SeatsScheme = SomeGdsProviderExtractor::extractObjects($object->SeatsScheme, SeatsSchemeElement::class);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'Name' => $this->Name,
            'Model' => $this->Model,
            'seatsAmount' => $this->SeatCapacity,
            'baggageAmount' => $this->BaggageCapacity,
            'seatsScheme' => $this->toArraySeatsScheme(),
        ];
    }

    /**
     * @return array|null
     */
    public function toArraySeatsScheme()
    {
        if (is_array($this->SeatsScheme)) {
            $seats = [];
            foreach ($this->SeatsScheme as $seat) {
                if ($seat instanceof SeatsSchemeElement) {
                    $seats[] = $seat->toArray();
                }
            }

            return $seats;
        }

        return null;
    }
}