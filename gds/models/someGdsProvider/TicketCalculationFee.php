<?php
namespace app\modules\gds\models\someGdsProvider;

class TicketCalculationFee extends GdsObject
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var float
     */
    public $Discount;
    /**
     * @var float
     */
    public $Amount;
    /**
     * @var boolean
     */
    public $CarriersFee;
}