<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class Services extends GdsObject
{
    /**
     * @var null|Service[]
     */
    public $Elements;
    /**
     * @var null|Cheque[]
     */
    public $Cheques;
    /**
     * @var null|string
     */
    public $Date;
    /**
     * @var null|string
     */
    public $Number;
    /**
     * @var null|PersonalData
     */
    public $PersonalData;
    /**
     * @var null|AdditionalAttribute[]
     */
    public $AdditionalAttributes;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);

        property_exists($object, 'PersonalData') && $this->PersonalData = SomeGdsProviderExtractor::extractObject($object->PersonalData, PersonalData::class);
        if (property_exists($object, 'Elements')) {
            $this->Elements = SomeGdsProviderExtractor::extractObjects($object->Elements, Service::class);
        }
        if (property_exists($object, 'Cheques')) {
            $this->Cheques = SomeGdsProviderExtractor::extractObjects($object->Cheques, Cheque::class);
        }
        if (property_exists($object, 'AdditionalAttributes')) {
            $this->AdditionalAttributes = SomeGdsProviderExtractor::extractObjects($object->AdditionalAttributes, AdditionalAttribute::class);
        }
    }
}