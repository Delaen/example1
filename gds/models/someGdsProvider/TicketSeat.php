<?php
namespace app\modules\gds\models\someGdsProvider;

class TicketSeat extends GdsObject
{
    /**
     * @var string
     */
    public $FareName;
    /**
     * @var int
     */
    public $SeatNum;
    /**
     * @var null|string
     */
    public $Destination;
    /**
     * @var null|string
     */
    public $TicketNumber;
    /**
     * @var null|int
     */
    public $ParentTicketSeatNum;
}