<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;
use yii\validators\DateValidator;

class RouteItem extends GdsObject
{
    /**
     * @var null|BusStop
     */
    public $BusStop;
    /**
     * @var null|float
     */
    public $Distance;
    /**
     * @var null|string
     */
    public $DepartureTime;
    /**
     * @var null|string
     */
    public $ArrivalTime;
    /**
     * @var null|int
     */
    public $StopDuration;
    /**
     * @var null|int
     */
    public $DayOfTrip;
    /**
     * @var null|int
     */
    public $Platform;
    /**
     * @var null|boolean
     */
    public $BanSaleFrom;
    /**
     * @var null|boolean
     */
    public $BanSaleTo;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'BusStop') && $this->BusStop = SomeGdsProviderExtractor::extractObject($object->BusStop, BusStop::class);
    }

    public function toArray()
    {
        $dateValidator = new DateValidator(['format' => 'php:Y-m-d\TH:i:s']);
        return [
            'busStop' => $this->BusStop->Address ?: $this->BusStop->Name,
            'departureTime' => $dateValidator->validate($this->DepartureTime) ? $this->DepartureTime : null,
            'arrivalTime' => $dateValidator->validate($this->ArrivalTime) ? $this->ArrivalTime : null,
            'stopDuration' => $this->StopDuration,
            'distance' => $this->Distance
        ];
    }
}
