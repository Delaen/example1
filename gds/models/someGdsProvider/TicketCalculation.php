<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class TicketCalculation extends GdsObject
{
    /**
     * @var null|float
     */
    public $FareDiscount;
    /**
     * @var float
     */
    public $FareAmount;
    /**
     * @var null|TicketCalculationFee[]
     */
    public $Fees;
    /**
     * @var null|float
     */
    public $TotalDiscount;
    /**
     * @var float
     */
    public $TotalAmount;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        if (property_exists($object, 'Fees')) {
            $this->Fees = SomeGdsProviderExtractor::extractObjects($object->Fees, TicketCalculationFee::class);
        }
    }
}