<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class Reserve extends GdsObject
{
    /**
     * @var null|Cheque[]|Cheque
     */
    public $Cheques;
    /**
     * @var null|string
     */
    public $ReserveKind;
    /**
     * @var null|float
     */
    public $ReserveCost;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        if (property_exists($object, 'Cheques')) {
            $this->Cheques = SomeGdsProviderExtractor::extractObjects($object->Cheques, Cheque::class);
        }
    }
}