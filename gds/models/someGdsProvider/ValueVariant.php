<?php
namespace app\modules\gds\models\someGdsProvider;

class ValueVariant extends GdsObject
{
    /**
     * @var null|string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $InputMask;
    public $ValueProperty1;
    public $ValueProperty2;
    public $ValueProperty3;
    public $ValueProperty4;
    public $ValueProperty5;

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->Name,
            'inputMask' => $this->InputMask,
        ];
    }

    /**
     * @param array $variants
     * @return array
     */
    static function toArrayMultiple(array $variants)
    {
        $result = [];
        foreach ($variants as $variant) {
            if ($variant instanceof ValueVariant) {
                $result[] = $variant->toArray();
            }
        }

        return $result;
    }
}