<?php
namespace app\modules\gds\models\someGdsProvider;

class PaymentItem extends GdsObject
{
    /**
     * @var string
     */
    public $PaymentType;
    /**
     * @var float
     */
    public $Amount;
    /**
     * @var null|string
     */
    public $TerminalNum;
    /**
     * @var null|string
     */
    public $TerminalChequeNum;
    /**
     * @var null|string
     */
    public $PaymentSystemOrderNum;
    /**
     * @var null|string
     */
    public $PaymentCardKind;
    /**
     * @var null|string
     */
    public $PaymentCardNum;
}