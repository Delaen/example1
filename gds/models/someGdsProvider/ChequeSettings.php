<?php
namespace app\modules\gds\models\someGdsProvider;

class ChequeSettings extends GdsObject
{
    /**
     * @var int
     */
    public $ChequeWidth;
    /**
     * @var null|boolean
     */
    public $TaggedText;
}