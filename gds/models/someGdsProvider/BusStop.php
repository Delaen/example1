<?php
namespace app\modules\gds\models\someGdsProvider;

use yii\web\HttpException;

class BusStop extends GdsObject
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var string
     */
    public $Code;
    /**
     * @var string
     */
    public $Id;
    /**
     * @var string
     */
    public $Country;
    /**
     * @var string
     */
    public $Region;
    /**
     * @var string
     */
    public $District;
    /**
     * @var boolean
     */
    public $Automated;
    /**
     * @var boolean
     */
    public $HasDestinations;
    /**
     * @var float
     */
    public $UTC;
    /**
     * @var string
     */
    public $GPSCoordinates;
    /**
     * @var string
     */
    public $LocationType;
    /**
     * @var string
     */
    public $Locality;
    /**
     * @var string
     */
    public $StoppingPlace;
    /**
     * @var string
     */
    public $Address;
    /**
     * @var string
     */
    public $Phone;

    /**
     * Merging values of attributes of both objects
     * @param BusStop $busStop
     */
    public function mergeAttributes(self $busStop)
    {
        if ($this->Id === $busStop->Id) {
            foreach ($this as $attr => $value) {
                !$value && $busStop->$attr && $this->$attr = $busStop->$attr;
            }
        }
    }
}