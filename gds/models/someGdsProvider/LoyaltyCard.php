<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class LoyaltyCard extends GdsObject
{
    /**
     * @var null|string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Number;
    /**
     * @var null|Customer
     */
    public $Owner;
    /**
     * @var null|string
     */
    public $ExpiryDate;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'Owner') && $this->Owner = SomeGdsProviderExtractor::extractObject($object->Owner, Customer::class);
    }
}