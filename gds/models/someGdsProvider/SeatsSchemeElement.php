<?php
namespace app\modules\gds\models\someGdsProvider;

class SeatsSchemeElement extends GdsObject
{
    /**
     * @var int
     */
    public $XPos;
    /**
     * @var int
     */
    public $YPos;
    /**
     * @var null|string
     */
    public $SeatNum;

    public function toArray()
    {
        return [
            'x' => $this->XPos,
            'y' => $this->YPos,
            'number' => $this->SeatNum,
        ];
    }
}