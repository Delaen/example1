<?php
namespace app\modules\gds\models\someGdsProvider;

class Service extends GdsObject
{
    /**
     * @var null|string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Description;
    /**
     * @var null|string
     */
    public $Measure;
    /**
     * @var null|float
     */
    public $Cost;
    /**
     * @var null|float
     */
    public $Quantity;
    /**
     * @var null|float
     */
    public $Amount;
    /**
     * @var null|boolean
     */
    public $ManuallyCost;
    /**
     * @var null|string
     */
    public $Number;
    /**
     * @var null|string
     */
    public $ParentDoc;
}