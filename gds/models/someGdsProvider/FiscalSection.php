<?php
namespace app\modules\gds\models\someGdsProvider;

class FiscalSection extends GdsObject
{
    /**
     * @var int
     */
    public $SectionNumber;
    /**
     * @var float
     */
    public $SectionSum;
    /**
     * @var null|float
     */
    public $Tax;
}