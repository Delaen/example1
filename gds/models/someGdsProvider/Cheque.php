<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class Cheque extends GdsObject
{
    /**
     * @var null|string
     */
    public $ChequeLines;
    /**
     * @var null|string
     */
    public $Barcode;
    /**
     * @var boolean
     */
    public $Fiscal;
    /**
     * @var float
     */
    public $FiscalSum;
    /**
     * @var null|string
     */
    public $Caption;
    /**
     * @var null|boolean
     */
    public $Sticker;
    /**
     * @var null|boolean
     */
    public $Printed;
    /**
     * @var null|FiscalSection
     */
    public $FiscalSection;
    /**
     * @var null|string
     */
    public $ChequeID;
    /**
     * @var null|string
     */
    public $DBDocNum;
    /**
     * @var null|string
     */
    public $ParentDoc;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'FiscalSection') && $this->FiscalSection = SomeGdsProviderExtractor::extractObject($object->FiscalSection, FiscalSection::class);
    }
}