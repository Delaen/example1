<?php
namespace app\modules\gds\models\someGdsProvider;

use yii\web\HttpException;

abstract class GdsObject
{
    /**
     * GdsObject constructor.
     * @param \stdClass $object
     * @param $vendor
     */
    public function __construct(\stdClass $object)
    {
        foreach (get_object_vars($object) as $property => $value) {
            property_exists(static::class, $property) && ($this->$property = $value);
        }
    }

    /**
     * @param $name
     * @throws HttpException
     */
    public function __get($name)
    {
        throw new HttpException(400, 'property doesn\'t exist');
    }

    /**
     * @param $name
     * @param $value
     * @throws HttpException
     */
    public function __set($name, $value)
    {
        throw new HttpException(400, 'property doesn\'t exist');
    }

    /**
     * @return object
     * @throws \ReflectionException
     */
    static function createObject()
    {
        $reflection = new \ReflectionClass(static::class);
        return $reflection->newInstanceWithoutConstructor();
    }
}