<?php
namespace app\modules\gds\models\someGdsProvider;

class BusSeat extends GdsObject
{
    /**
     * @var string
     */
    public $Type;
    /**
     * @var int
     */
    public $Number;
    /**
     * @var null|string
     */
    public $Status;
    /**
     * @var null|int
     */
    public $ParentTicketSeatNum;
    /**
     * @var null|boolean
     */
    public $ForCurrentOrder;

    public function toArray()
    {
        return [
            'isPassenger' => $this->Type === 'Passenger' ? true : false,
            'seatNumber' => $this->Number,
            'status' => in_array($this->Status, Constant::busSeatsStatuses()) ? $this->Status : null,
        ];
    }

    /**
     * @param BusSeat[] $seats
     * @return array
     */
    static function toArrayMultiple(array $seats)
    {
        $result = [];
        foreach ($seats as $seat) {
            if ($seat instanceof static) {
                $result[] = $seat->toArray();
            }
        }

        return $result;
    }
}