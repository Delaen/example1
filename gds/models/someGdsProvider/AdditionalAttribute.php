<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class AdditionalAttribute extends GdsObject
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Caption;
    /**
     * @var null|bool
     */
    public $Mandatory;
    /**
     * @var null|string
     */
    public $Type;
    /**
     * @var null|ValueVariant[]|ValueVariant
     */
    public $ValueVariants;
    /**
     * @var null|string
     */
    public $InputMask;
    /**
     * @var null|string
     */
    public $Value;
    /**
     * @var null|boolean
     */
    public $CostAttribute;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);

        if (property_exists($object, 'ValueVariants')) {
            $this->ValueVariants = SomeGdsProviderExtractor::extractObjects($object->ValueVariants, ValueVariant::class);
        }
    }
}