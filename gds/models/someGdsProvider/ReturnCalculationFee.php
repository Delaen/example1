<?php
namespace app\modules\gds\models\someGdsProvider;

class ReturnCalculationFee extends GdsObject
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var float
     */
    public $SumToReturn;
    /**
     * @var float
     */
    public $SumInTicket;
}