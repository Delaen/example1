<?php

namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;
use yii\validators\DateValidator;
use yii\web\HttpException;

class Trip extends GdsObject
{
    /**
     * @var null|string
     */
    public $Id;
    /**
     * @var null|string
     */
    public $RouteId;
    /**
     * @var null|string
     */
    public $ScheduleTripId;
    /**
     * @var null|string
     */
    public $RouteName;
    /**
     * @var null|string
     */
    public $RouteNum;
    /**
     * @var null|string
     */
    public $Carrier;
    /**
     * @var null|Bus
     */
    public $Bus;
    /**
     * @var null|string
     */
    public $Driver1;
    /**
     * @var null|string
     */
    public $Driver2;
    /**
     * @var null|string
     */
    public $Frequency;
    /**
     * @var null|string
     */
    public $WaybillNum;
    /**
     * @var null|string
     */
    public $Status;
    /**
     * @var null|string
     */
    public $StatusPrint;
    /**
     * @var null|string
     */
    public $StatusReason;
    /**
     * @var null|string
     */
    public $StatusComment;
    /**
     * @var null|string
     */
    public $StatusDate;
    /**
     * @var null|BusStop
     */
    public $Departure;
    /**
     * @var null|string
     */
    public $DepartureTime;
    /**
     * @var null|string
     */
    public $ArrivalToDepartureTime;
    /**
     * @var null|BusStop
     */
    public $Destination;
    /**
     * @var null|string
     */
    public $ArrivalTime;
    /**
     * @var null|float
     */
    public $Distance;
    /**
     * @var null|int
     */
    public $Duration;
    /**
     * @var null|bool
     */
    public $TransitSeats;
    /**
     * @var null|int
     */
    public $FreeSeatsAmount;
    /**
     * @var null|float
     */
    public $PassengerFareCost;
    /**
     * @var null|Fare|Fare[]
     */
    public $Fares;
    /**
     * @var null|int
     */
    public $Platform;
    /**
     * @var null|bool
     */
    public $OnSale;
    /**
     * @var null|RouteItem|RouteItem[]
     */
    public $Route;
    /**
     * @var null|bool
     */
    public $Additional;
    /**
     * @var null|RouteItem
     */
    public $AdditionalTripTime;
    /**
     * @var null|bool
     */
    public $TransitTrip;
    /**
     * @var null|string
     */
    public $SaleStatus;
    /**
     * @var null|bool
     */
    public $ACBPDP;
    /**
     * @var null|string
     */
    public $FactTripReturnTime;
    /**
     * @var null|string
     */
    public $Currency;
    /**
     * @var null|string
     */
    public $PrincipalTaxId;
    /**
     * @var null|string
     */
    public $Comment;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'Bus') && $this->Bus = SomeGdsProviderExtractor::extractObject($object->Bus, Bus::class);
        property_exists($object, 'Departure') && $this->Departure = SomeGdsProviderExtractor::extractObject($object->Departure, BusStop::class);
        property_exists($object, 'Destination') && $this->Destination = SomeGdsProviderExtractor::extractObject($object->Destination, BusStop::class);

        if (property_exists($object, 'Fares')) {
            $this->Fares = SomeGdsProviderExtractor::extractObjects($object->Fares, Fare::class);
        }

        if (property_exists($object, 'Route')) {
            $this->Route = SomeGdsProviderExtractor::extractObjects($object->Route, RouteItem::class);
        }
    }

    public function toArray(): ?array
    {
        $dateValidator = new DateValidator(['format' => 'php:Y-m-d\TH:i:s']);

        //Если не передан arrivalTime, то получаем его из маршрута
        if(!$this->ArrivalTime && is_array($this->Route)) {
            foreach ($this->Route as $busStop) {
                if (($busStop instanceof RouteItem) && $busStop->BusStop->Id === $this->Destination->Id) {
                    $this->ArrivalTime = ($dateValidator->validate($busStop->ArrivalTime) ? $busStop->ArrivalTime : null);
                }
            }
        }

        return [
            'departure' => ($this->Departure instanceof BusStop) ? $this->Departure->Name : null,
            'departureAddress' => ($this->Departure instanceof BusStop) ? $this->Departure->Address : null,
            'departureTime' => $dateValidator->validate($this->DepartureTime) ? $this->DepartureTime : null,
            'destination' => ($this->Destination instanceof BusStop) ? $this->Destination->Name : null,
            'destinationAddress' => ($this->Destination instanceof BusStop) ? $this->Destination->Address : null,
            'arrivalTime' => $dateValidator->validate($this->ArrivalTime) ? $this->ArrivalTime : null,
            'carrier' => $this->Carrier,
            'routeNum' => $this->RouteNum,
            'routeName' => $this->RouteName,
            'freeSeatsAmount' => $this->FreeSeatsAmount,
            'bus' => ($this->Bus instanceof Bus) ? $this->Bus->toArray() : null,
            'fares' => $this->toArrayFares()
        ];
    }

    /**
     * @return array
     */
    public function toArrayFares(): array
    {
        $fares = [];
        if (is_array($this->Fares)) {
            foreach ($this->Fares as $fare) {
                if (($fare instanceof Fare) && in_array($fare->Name, Constant::fareTypes(true)) && $fare->Cost != 0) {
                    $fareArray = $fare->toArray();
                    $fareArray && $fares[] = $fareArray;
                }
            }
        } elseif ($this->Fares instanceof Fare) {
            if (($this->Fares instanceof Fare) && in_array($this->Fares->Name, Constant::fareTypes(true)) && $this->Fares->Cost != 0) {
                $fareArray = $this->Fares->toArray();
                $fareArray && $fares[] = $fareArray;
            }
        }

        return $fares;
    }

    /**
     * @return array|null
     */
    public function toArrayRouteItems(): ?array
    {
        if (is_array($this->Route)) {
            $route = [];
            foreach ($this->Route as $routeItem) {
                if ($routeItem instanceof RouteItem) {
                    $route[] = $routeItem->toArray();
                }
            }

            return $route;
        }

        return null;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function toArrayForTripList()
    {
        $duration = null;
        $dateValidator = new DateValidator(['format' => 'php:Y-m-d\TH:i:s']);
        if ($dateValidator->validate($this->DepartureTime) && $dateValidator->validate($this->ArrivalTime)) {
            $dateInterval = (new \DateTime($this->DepartureTime))->diff(new \DateTime($this->ArrivalTime));
            if ($dateInterval->f === 0.0 && $dateInterval->invert === 0) {
                $duration = [
                    'days' => $dateInterval->d,
                    'hours' => $dateInterval->h,
                    'minutes' => $dateInterval->i,
                ];
            }
        }

        return [
            'tripId' => $this->Id,
            'routeNum' => $this->RouteNum,
            'duration' => $duration,
            'departureTime' => $this->DepartureTime,
            'departureName' => $this->Departure->Name,
            'departureAddress' => $this->Departure->Address,
            'arrivalTime' => $this->ArrivalTime,
            'destinationName' => $this->Destination->Name,
            'destinationAddress' => $this->Destination->Address,
            'seatsAmount' => ($this->Bus instanceof Bus) ? $this->Bus->SeatCapacity : null,
            'freeSeatsAmount' => $this->FreeSeatsAmount,
            'cost' => $this->PassengerFareCost,
            'carrier' => $this->Carrier,
            'comment' => $this->Comment,
        ];
    }

    /**
     * @param array $trips
     * @return array
     * @throws \Exception
     */
    static function toArrayForTripListMultiple(array $trips): array
    {
        $result = [];
        foreach ($trips as $trip) {
            if ($trip instanceof Trip) {
                $result[] = $trip->toArrayForTripList();
            }
        }

        return $result;
    }
}
