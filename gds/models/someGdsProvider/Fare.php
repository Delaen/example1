<?php
namespace app\modules\gds\models\someGdsProvider;

class Fare extends GdsObject
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Caption;
    /**
     * @var null|string
     */
    public $SeatType;
    /**
     * @var null|int
     */
    public $LowAgeLimit;
    /**
     * @var null|int
     */
    public $HighAgeLimit;
    /**
     * @var null|boolean
     */
    public $OnlyWithPassenger;
    /**
     * @var null|float
     */
    public $Cost;

    /**
     * @return array|null
     */
    public function toArray(): ?array
    {
        $type = '';
        switch ($this->Name) {
            case 'Пассажирский' :
                $type = Constant::FARE_TYPE_FULL;
                break;
            case 'Детский':
                $type = Constant::FARE_TYPE_CHILD;
                break;
            case 'Багажный':
                $type = Constant::FARE_TYPE_BAGGAGE;
                break;
            default:
                return null;
        }

        return [
            'type' => $type,
            'cost' => $this->Cost,
            'solo' => !$this->OnlyWithPassenger,
        ];
    }
}