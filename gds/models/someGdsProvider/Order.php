<?php

namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\components\someGdsProvider\SaleSession;
use app\modules\gds\helpers\SomeGdsProviderExtractor;

class Order extends GdsObject
{
    /**
     * @var string
     */
    public $Number;
    /**
     * @var null|Trip
     */
    public $Trip;
    /**
     * @var null|BusStop
     */
    public $Departure;
    /**
     * @var null|string
     */
    public $DepartureTime;
    /**
     * @var null|BusStop
     */
    public $Destination;
    /**
     * @var null|Ticket[]|Ticket
     */
    public $Tickets;
    /**
     * @var null|BusSeat[]|BusSeat
     */
    public $OccupiedSeats;
    /**
     * @var null|float
     */
    public $Amount;
    /**
     * @var null|Customer
     */
    public $Customer;
    /**
     * @var null|Service|Service[]
     */
    public $Services;
    /**
     * @var null|int
     */
    public $SecondsToUnlockSeats;
    /**
     * @var null|Reserve
     */
    public $Reserve;
    /**
     * @var null|LoyaltyCard
     */
    public $LoyaltyCard;
    /**
     * @var null|string
     */
    public $Currency;

    /**
     * Order constructor.
     * @param \stdClass $object
     */
    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'Trip') && $this->Trip = SomeGdsProviderExtractor::extractObject($object->Trip, Trip::class);
        property_exists($object, 'Departure') && $this->Departure = SomeGdsProviderExtractor::extractObject($object->Departure, BusStop::class);
        property_exists($object, 'Destination') && $this->Destination = SomeGdsProviderExtractor::extractObject($object->Destination, BusStop::class);
        property_exists($object, 'Customer') && $this->Customer = SomeGdsProviderExtractor::extractObject($object->Customer, Customer::class);
        property_exists($object, 'Reserve') && $this->Reserve = SomeGdsProviderExtractor::extractObject($object->Reserve, Reserve::class);
        property_exists($object, 'LoyaltyCard') && $this->LoyaltyCard = SomeGdsProviderExtractor::extractObject($object->LoyaltyCard, LoyaltyCard::class);

        if (property_exists($object, 'Tickets')) {
            $this->Tickets = SomeGdsProviderExtractor::extractObjects($object->Tickets, Ticket::class);
        }

        if (property_exists($object, 'OccupiedSeats')) {
            $this->OccupiedSeats = SomeGdsProviderExtractor::extractObjects($object->OccupiedSeats, BusSeat::class);
        }

        if (property_exists($object, 'Services')) {
            $this->Services = SomeGdsProviderExtractor::extractObjects($object->Services, Service::class);
        }

    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        //==========Добавляем статусы в Схему посадки автобуса ===========
        $occupiedSeats = ($this->OccupiedSeats instanceof BusSeat) ? [$this->OccupiedSeats->toArray()] : (is_array($this->OccupiedSeats) ? BusSeat::toArrayMultiple($this->OccupiedSeats) : []);
        $tripArray = ($this->Trip instanceof Trip) ? $this->Trip->toArray() : null;

        if (is_array($tripArray) && is_array($occupiedSeats) && is_array($tripArray['bus']) &&
            is_array($tripArray['bus']['seatsScheme'])) {
            $occupiedStatuses = [];
            foreach ($occupiedSeats as $seat) {
                $seat['isPassenger'] && $occupiedStatuses[$seat['seatNumber']] = $seat['status'];
            }

            foreach ($tripArray['bus']['seatsScheme'] as &$seat) {
                if ($seat['number'] != 0) {
                    $seat['status'] = array_key_exists($seat['number'], $occupiedStatuses) ?
                        $occupiedStatuses[$seat['number']] :
                        Constant::BUS_SEAT_FREE;
                }
            }
        }
        //=================================================================
        $tickets = [];
        if (is_array($this->Tickets)) {
            $tickets = Ticket::toArrayMultiple($this->Tickets);
        } elseif ($this->Tickets instanceof Ticket) {
            $tickets = $this->Tickets->toArray();
            $tickets = [$tickets['type'] => [$tickets]];
        }

        return [
            'number' => $this->Number,
            'trip' => $tripArray,
            //'occupiedSeats' => $occupiedSeats,
            'tickets' => $tickets,
            'amount' => $this->Amount,
        ];
    }

    /**
     * @return bool
     */
    public function checkTicketsPersonalData()
    {
        $tickets = $this->getTickets();
        if (!empty($tickets)) {
            foreach ($tickets as $ticket) {
                if (!$ticket->checkPersonalData()) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function checkSeatNumbers()
    {
        $tickets = $this->getTickets();
        $numbers = [];
        $fares = Constant::fareTypes(true);
        unset($fares[Constant::FARE_TYPE_BAGGAGE]);
        $fares = array_values($fares);

        if (!empty($tickets)) {
            foreach ($tickets as $ticket) {
                if (in_array($ticket->FareName, $fares)) {
                    $numbers[] = $ticket->SeatNum;
                }
            }
        }

        return count($numbers) == count(array_unique($numbers));
    }

    /**
     * @param string $ticketNumber
     * @return bool
     */
    public function checkTicketExistence(string $ticketNumber)
    {
        foreach ($this->getTickets() as $ticket) {
            if ($ticket->Number === $ticketNumber) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $seatNumber
     * @return bool
     */
    public function checkSeatNumberExistence(string $seatNumber)
    {
        foreach ($this->getTickets() as $ticket) {
            if ($ticket->SeatNum === $seatNumber) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $ticketNumber
     * @return Ticket|Ticket[]|null
     */
    public function getTicket($ticketNumber)
    {
        foreach ($this->getTickets() as $ticket) {
            if ($ticket->Number == $ticketNumber) {
                return $ticket;
            }
        }

        return null;
    }

    /**
     * @return Ticket[]
     */
    public function getTickets()
    {
        if (is_array($this->Tickets)) {
            return $this->Tickets;
        } elseif ($this->Tickets instanceof Ticket) {
            return [$this->Tickets];
        }

        return [];
    }

    /**
     * Deleting ticket from order
     * @param $ticketNumber
     */
    public function deleteTicket($ticketNumber)
    {
        if (is_array($this->Tickets)) {
            foreach ($this->Tickets as $key => $ticket) {
                if ($ticket->Number == $ticketNumber) {
                    unset($this->Tickets[$key]);
                } elseif ($this->Tickets instanceof Ticket) {
                    ($ticket->Number == $ticketNumber) && $this->Tickets = null;
                }
            }
        }
    }

    /**
     * @return float
     */
    public function getTotalCost(): float
    {
        $cost = 0.0;

        foreach ($this->getTickets() as $ticket) {
            $cost += $ticket->getTotalCost();
        }

        return $cost;
    }

    /**
     * @param Order $replicatingOrder
     * @param bool $checkOccupation параметр указывающий делать ли доп.запрос к авибусу в случае отсутствия
     * @return $this
     * @throws \yii\web\HttpException
     */
    public function copy(self $replicatingOrder, bool $checkOccupation = false): self
    {
        $order = $replicatingOrder;
        !$order->Trip->Bus->SeatsScheme && $order->Trip->Bus->SeatsScheme = $this->Trip->Bus->SeatsScheme;
        if (!$order->OccupiedSeats) {
            if ($checkOccupation) {
                $saleSession = SaleSession::getInstance();
                $order->OccupiedSeats = $saleSession->getOccupiedSeats();
            } else {
                $order->OccupiedSeats = $this->OccupiedSeats;
            }
        }

        return $order;
    }
}