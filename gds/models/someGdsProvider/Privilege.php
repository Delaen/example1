<?php
namespace app\modules\gds\models\someGdsProvider;

class Privilege extends GdsObject
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Caption;
    /**
     * @var null|int
     */
    public $Limit;
}