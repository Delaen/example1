<?php

namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class Ticket extends GdsObject
{
    /**
     * @var string
     */
    public $Number;
    /**
     * @var null|string
     */
    public $Date;
    /**
     * @var null|string
     */
    public $TripId;
    /**
     * @var null|string
     */
    public $Carrier;
    /**
     * @var null|int
     */
    public $ParentTicketSeatNum;
    /**
     * @var null|string
     */
    public $SeatType;
    /**
     * @var null|string
     */
    public $SeatNum;
    /**
     * @var null|string
     */
    public $FareName;
    /**
     * @var null|string
     */
    public $PrivilageName;
    /**
     * @var null|TicketCalculation
     */
    public $Calculation;
    /**
     * @var null|BusStop
     */
    public $Departure;
    /**
     * @var null|string
     */
    public $DepartureTime;
    /**
     * @var null|BusStop
     */
    public $Destination;
    /**
     * @var null|string
     */
    public $ArrivalTime;
    /**
     * @var null|float
     */
    public $Distance;
    /**
     * @var null|string
     */
    public $PassengerName;
    /**
     * @var null|string
     */
    public $PassengerDoc;
    /**
     * @var null|PersonalData|PersonalData[]
     */
    public $PersonalData;
    /**
     * @var null|AdditionalAttribute|AdditionalAttribute[]
     */
    public $AdditionalAttributes;
    /**
     * @var Cheque[]|Cheque
     */
    public $Cheques;
    /**
     * @var null|boolean
     */
    public $Absence;
    /**
     * @var null|float
     */
    public $FaultDistance;
    /**
     * @var null|string
     */
    public $FaultCarrier;
    /**
     * @var null|boolean
     */
    public $PaymentOnTheBoarding;
    /**
     * @var null|Customer
     */
    public $Customer;
    /**
     * @var null|MarketingCampaign
     */
    public $MarketingCampaign;
    /**
     * @var null|float
     */
    public $BusstationFee;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'Calculation') && $this->Calculation = SomeGdsProviderExtractor::extractObject($object->Calculation, TicketCalculation::class);
        property_exists($object, 'Departure') && $this->Departure = SomeGdsProviderExtractor::extractObject($object->Departure, BusStop::class);
        property_exists($object, 'Destination') && $this->Destination = SomeGdsProviderExtractor::extractObject($object->Destination, BusStop::class);
        property_exists($object, 'Customer') && $this->Customer = SomeGdsProviderExtractor::extractObject($object->Customer, Customer::class);
        property_exists($object, 'MarketingCampaign') && $this->MarketingCampaign = SomeGdsProviderExtractor::extractObject($object->MarketingCampaign, MarketingCampaign::class);

        if (property_exists($object, 'PersonalData')) {
            $this->PersonalData = SomeGdsProviderExtractor::extractObjects($object->PersonalData, PersonalData::class);
        }

        if (property_exists($object, 'AdditionalAttributes')) {
            $this->AdditionalAttributes = SomeGdsProviderExtractor::extractObjects($object->AdditionalAttributes, AdditionalAttribute::class);
        }

        if (property_exists($object, 'Cheques')) {
            $this->Cheques = SomeGdsProviderExtractor::extractObjects($object->Cheques, Cheque::class);
        }
    }

//    private function checkAge(PersonalData $personalData)
//    {
//        if (mb_strtolower($this->FareName) == 'детский' && $personalData->Name == 'Дата рождения') {
//            $now = new \DateTime();
//            $birthDate = new \DateTime($personalData->Value);
//            var_dump($now->diff($birthDate));die;
//            return false;
//        }
//
//        return true;
//    }

    /**
     * @param $data
     * @throws \ReflectionException
     */
    public function setPersonalData($data)
    {
        $fio = PersonalData::createObject();
        $fio->Name = 'ФИО';
        $fio->Value = isset($data['fio']) ? $data['fio'] : '';

        $document = PersonalData::createObject();
        $document->Name = 'Удостоверение';
        $document->ValueKind = isset($data['docType']) ? $data['docType'] : '';
        $document->Value = isset($data['docNumber']) ? $data['docNumber'] : '';

        $birthday = PersonalData::createObject();
        $birthday->Name = 'Дата рождения';
        $birthday->Value = isset($data['birthday']) ? $data['birthday'] : '';

        $gender = PersonalData::createObject();
        $gender->Name = 'Пол';
        $gender->Value = isset($data['gender']) ? $data['gender'] : '';

        $nationality = PersonalData::createObject();
        $nationality->Name = 'Гражданство';
        $nationality->Value = isset($data['nationality']) ? $data['nationality'] : '';

        $phone = PersonalData::createObject();
        $phone->Name = 'Мобильный телефон';
        $phone->Value = isset($data['phone']) ? $data['phone'] : '';
        $phone->ValueKind = 'Мобильный телефон';

        $privilege = PersonalData::createObject();
        $privilege->Name = isset($data['privilegeDoc']) ? $data['privilegeDoc'] : '';
        $privilege->Value = isset($data['privilegeDocValue']) ? $data['privilegeDocValue'] : '';

        $this->PersonalData = [$fio, $document, $birthday, $nationality, $gender, $phone, $privilege];
    }

    /**
     * Устанавливаем данные по номеру билета и типу тарифа
     * @param $data
     */
    public function setMetaData($data)
    {
        isset($data['seatNumber']) && $this->SeatNum = $data['seatNumber'];
        if (isset($data['type']) && in_array($data['type'], Constant::fareTypes())) {
            $this->FareName = Constant::fareTypes(true)[$data['type']];
        }
    }

    /**
     * @return bool
     */
    public function checkPersonalData()
    {
        if (is_array($this->PersonalData)) {
            foreach ($this->PersonalData as $item) {
                if (!$item->checkData()) {
                    return false;
                }
            }

            return true;
        } elseif ($this->PersonalData instanceof PersonalData) {
            return $this->PersonalData->checkData();
        }

        return true;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $personalData = [];
        if (is_array($this->PersonalData)) {
            foreach ($this->PersonalData as $dataItem) {
                if ($dataItem instanceof PersonalData) {
                    $data = $dataItem->toArray(true);
                    $personalData[$data['type']] = $data;
                }
            }
        }elseif ($this->PersonalData instanceof PersonalData) {
            $result = $this->PersonalData->toArray(true);
            $personalData = ['type' => $result];
        }

        return [
            'number' => $this->Number,
            'type' => array_flip(Constant::fareTypes(true))[$this->FareName],
            'seatNumber' => $this->SeatNum,
            'tax' => $this->getTaxAmount(),
            'totalCost' => $this->Calculation->TotalAmount,
            'privilege' => $this->PrivilageName,
            'personalData' => $personalData
        ];
    }

    /**
     * @return float
     */
    public function getTotalCost():float
    {
        return (float) $this->Calculation->TotalAmount;
    }

    /**
     * @return float
     */
    public function getTaxAmount():float
    {
        return (float) $this->Calculation->TotalAmount - $this->Calculation->FareAmount;
    }

    /**
     * @return array
     */
    public function getAllPersonalData()
    {
        $result = [];
        if (is_array($this->PersonalData)) {
            foreach ($this->PersonalData as $data) {
                $dataArray = $data->toArray();
                array_key_exists('type', $dataArray) && $result[$dataArray['type']] = $dataArray;
            }
        }elseif (($this->PersonalData instanceof PersonalData)) {
            $dataArray = $this->PersonalData->toArray();
            array_key_exists('type', $dataArray) && $result[$dataArray['type']] = $dataArray;
        }

        return $result;
    }

    /**
     * @param array $tickets
     * @return array
     */
    static function toArrayMultiple(array $tickets)
    {
        $result = [];

        foreach ($tickets as $ticket) {
            if ($ticket instanceof Ticket) {
                $data = $ticket->toArray();
                $result[$data['type']][] = $data;
            }
        }

        return $result;
    }
}
