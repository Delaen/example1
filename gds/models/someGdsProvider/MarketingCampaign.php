<?php
namespace app\modules\gds\models\someGdsProvider;

class MarketingCampaign extends GdsObject
{
    /**
     * @var null|string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Id;
}