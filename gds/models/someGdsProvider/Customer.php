<?php
namespace app\modules\gds\models\someGdsProvider;

class Customer extends GdsObject
{
    /**
     * @var null|string
     */
    public $Name;
    /**
     * @var null|string
     */
    public $Phone;
    /**
     * @var null|string
     */
    public $Email;
    /**
     * @var null|string
     */
    public $Comment;
}