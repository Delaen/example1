<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;

class PaymentDoc extends GdsObject
{
    /**
     * @var string
     */
    public $Number;
    /**
     * @var null|string
     */
    public $Date;
    /**
     * @var string
     */
    public $OrderNumber;
    /**
     * @var null|Ticket[]
     */
    public $Tickets;
    /**
     * @var null|Services
     */
    public $Services;
    /**
     * @var null|LoyaltyCard
     */
    public $LoyaltyCard;
    /**
     * @var null|BusStop
     */
    public $Departure;
    /**
     * @var null|float
     */
    public $Amount;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'Services') && $this->Services = SomeGdsProviderExtractor::extractObject($object->Services, Services::class);
        property_exists($object, 'LoyaltyCard') && $this->LoyaltyCard = SomeGdsProviderExtractor::extractObject($object->LoyaltyCard, LoyaltyCard::class);
        property_exists($object, 'Departure') && $this->Departure = SomeGdsProviderExtractor::extractObject($object->Departure, BusStop::class);

        if (property_exists($object, 'Tickets')) {
            $this->Tickets = SomeGdsProviderExtractor::extractObjects($object->Tickets, Ticket::class);
        }
    }
}