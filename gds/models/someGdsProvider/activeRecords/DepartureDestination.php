<?php

namespace app\modules\gds\models\someGdsProvider\activeRecords;

use app\modules\gds\models\someGdsProvider\Bus;
use Yii;

/**
 * This is the model class for table "some_gds_provider_departures_destinations".
 *
 * @property string $id_departure
 * @property string $id_destination
 *
 * @property BusStop $departure
 * @property BusStop $destination
 */
class DepartureDestination extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'some_gds_provider_departures_destinations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_departure', 'id_destination'], 'required'],
            [['id_departure', 'id_destination'], 'string'],
            [['id_departure', 'id_destination'], 'unique', 'targetAttribute' => ['id_departure', 'id_destination']],
            [['id_departure'], 'exist', 'skipOnError' => true, 'targetClass' => BusStop::className(), 'targetAttribute' => ['id_departure' => 'id']],
            [['id_destination'], 'exist', 'skipOnError' => true, 'targetClass' => BusStop::className(), 'targetAttribute' => ['id_destination' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_departure' => 'Id Departure',
            'id_destination' => 'Id Destination',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeparture()
    {
        return $this->hasOne(BusStop::className(), ['id' => 'id_departure']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(BusStop::className(), ['id' => 'id_destination']);
    }
}
