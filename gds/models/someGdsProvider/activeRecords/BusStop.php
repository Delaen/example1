<?php

namespace app\modules\gds\models\someGdsProvider\activeRecords;

use aracoool\uuid\Uuid;
use aracoool\uuid\UuidBehavior;
use nanson\postgis\behaviors\GeometryBehavior;
use Yii;

/**
 * This is the model class for table "integration_bus_stops".
 *
 * @property string $id
 * @property string $gds_id
 * @property string $name
 * @property string $country
 * @property string $region
 * @property string $district
 * @property string $locality
 * @property string $coordinates
 * @property string $vendor
 * @property bool $automated
 * @property boolean $has_destinations
 */
class BusStop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%some_gds_provider_bus_stops}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'string'],
            [['gds_id', 'name', 'country', 'region', 'district', 'locality'], 'string', 'max' => 255],
            [['id', 'gds_id'], 'unique'],
            [['automated', 'has_destinations'], 'boolean'],
            [['coordinates'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => UuidBehavior::class,
                'version' => Uuid::V4,
            ],
            [
                'class' => GeometryBehavior::className(),
                'type' => GeometryBehavior::GEOMETRY_POINT,
                'attribute' => 'coordinates',
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gds_id' => 'Gds ID',
            'name' => 'Name',
            'country' => 'Country',
            'region' => 'Region',
            'district' => 'District',
            'locality' => 'Locality',
            'coordinates' => 'Coordinates',
        ];
    }

    /**
     * @param \app\modules\gds\models\someGdsProvider\BusStop $busStop
     */
    public function loadFromGds(\app\modules\gds\models\someGdsProvider\BusStop $busStop)
    {
        foreach (get_object_vars($busStop) as $key => $value) {
            $lKey = lcfirst($key);

            if ($lKey === 'id') {
                $this->gds_id = $value;
            }elseif ($lKey === 'hasDestinations') {
                $this->has_destinations = $value;
            }else {
                array_key_exists($lKey, $this->attributes) && $this->$lKey = $value;

                $coordinates = explode(',', $value); //Фикс кривых координат (Из авибуса иногда приходят кривые координаты)
                $key === 'GPSCoordinates' && $busStop->{'GPSCoordinates'} && $this->coordinates = [$coordinates[0], $coordinates[1]];
            }
        }
    }

    public function asArray()
    {
        return parent::toArray(['id', 'name']);
    }
}
