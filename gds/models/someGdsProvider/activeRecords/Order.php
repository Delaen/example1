<?php

namespace app\modules\gds\models\someGdsProvider\activeRecords;

use app\helpers\DateHelper;
use app\modules\gds\components\interfaces\OrderRecordInterface;
use app\modules\gds\helpers\SomeGdsProviderExtractor;
use app\modules\gds\helpers\SomeGdsProviderHelper;
use app\modules\gds\models\someGdsProvider\Constant;
use Yii;
use app\models\User;
use yii\behaviors\TimestampBehavior;
use yii\web\HttpException;

/**
 * This is the model class for table "some_gds_provider_order".
 *
 * @property int $id
 * @property int $user_id
 * @property \app\modules\gds\models\someGdsProvider\Order $order_data
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property TicketReturn $ticketReturns
 */
class Order extends \yii\db\ActiveRecord implements OrderRecordInterface
{
    /**
     * @var array
     */
    private $_returnedTickets = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'some_gds_provider_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'order_data', 'status'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['order_data'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => Constant::ORDER_STATUS_NOT_PAID],
            [['status'], 'in', 'range' => Constant::orderStatuses()],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'order_data' => 'Order Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function beforeSave($insert)
    {
        if (!($this->order_data instanceof \app\modules\gds\models\someGdsProvider\Order)) {
            throw new HttpException(500, 'Not Order instance');
        }

        $this->order_data = json_encode($this->order_data);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->order_data = SomeGdsProviderExtractor::extractObject(json_decode($this->order_data), \app\modules\gds\models\someGdsProvider\Order::class);
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        $this->order_data = SomeGdsProviderExtractor::extractObject(json_decode($this->order_data), \app\modules\gds\models\someGdsProvider\Order::class);
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketReturns()
    {
        return $this->hasMany(TicketReturn::className(), ['order_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getReturnTicketsNumbers()
    {
        if (!$this->_returnedTickets) {
            foreach ($this->ticketReturns as $ticket) {
                $this->_returnedTickets[] = $ticket->ticket_number;
            }
        }

        return $this->_returnedTickets;
    }

    /**
     * @param $ticketNumber
     * @return bool
     */
    public function isTicketReturned(string $ticketNumber): bool
    {
        foreach ($this->ticketReturns as $ticket) {
            if (($ticket->ticket_number == $ticketNumber) && $ticket->accepted) {
                return true;
            }
        }
        return false;
    }

    //====================== OrderRecordInterface implementation ==============

    /**
     * @return string
     */
    public function getIndex(): string
    {
        return \app\modules\gds\models\Order::GDS_SOME_PROVIDER . '_' . $this->id;
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->order_data->Number;
    }

    /**
     * @param bool $withTime
     * @return false|string
     */
    public function getCreated($withTime = false)
    {
        return date('d.m.Y ' . ($withTime ? 'H:i' : ''), $this->created_at);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        return Constant::orderStatuses(true)[$this->status];
    }

    /**
     * @return string
     */
    public function getRouteString()
    {
        return $this->order_data->Trip->RouteName;
    }

    /**
     * @return string|null
     * @throws HttpException
     */
    public function getDepartureId(): ?string
    {
        return SomeGdsProviderHelper::actualBusStopId($this->order_data->Departure->Id);
    }

    /**
     * @return string
     */
    public function getDepartureString()
    {
        return $this->order_data->Trip->Departure->Name;
    }

    /**
     * @return string
     */
    public function getDepartureAddress()
    {
        return $this->order_data->Trip->Departure->Address;
    }

    /**
     * @return string|null
     * @throws HttpException
     */
    public function getDestinationId(): ?string
    {
        return SomeGdsProviderHelper::actualBusStopId($this->order_data->Destination->Id);
    }

    /**
     * @return string
     */
    public function getDestinationString()
    {
        return $this->order_data->Destination->Name;
    }

    /**
     * @return string
     */
    public function getDestinationAddress()
    {
        return $this->order_data->Destination->Address;
    }

    /**
     * @return false|string
     */
    public function getDepartureTime()
    {
        return date('d.m.Y H:i', strtotime($this->order_data->DepartureTime));
    }

    /**
     * @return false|string
     */
    public function getDepartureTimeString()
    {
        return DateHelper::cyrillicDate(date('j F Y в G:i', strtotime($this->order_data->DepartureTime)));
    }

    /**
     * @return false|string
     */
    public function getDestinationTimeString()
    {
        return date('d.m.Y H:i', strtotime($this->order_data->Trip->ArrivalTime));
    }

    /**
     * @return string|void|null
     */
    public function getBusString()
    {
        return $this->order_data->Trip->Bus->Name;
    }

    /**
     * @return string|void|null
     */
    public function getCarrier()
    {
        return $this->order_data->Trip->Carrier;
    }

    /**
     * @return array
     */
    public function getTickets(): array
    {
        $tickets = [];

        foreach ($this->order_data->getTickets() as $ticket) {
            $personalData = $ticket->getAllPersonalData();

            $tickets[$ticket->Number] = [
                'number' => $ticket->Number,
                'seatNum' => $ticket->SeatNum,
                'fio' => array_key_exists('fio', $personalData) ? $personalData['fio']['value'] : '',
                'cost' => $ticket->getTotalCost(),
                'tax' => $ticket->getTaxAmount(),
                'fareName' => $ticket->FareName,
                'boardingPassType' => in_array($ticket->FareName, ['Пассажирский', 'Детский'])?
                    \app\models\constants\Constant::BOARDING_PASS_TYPE_PASSENGER :
                    \app\models\constants\Constant::BOARDING_PASS_TYPE_BAGGAGE
                ,
                'nationality' => array_key_exists(Constant::DOC_NATIONALITY, $personalData) ? $personalData[Constant::DOC_NATIONALITY]['value'] : '',
                'gender' => array_key_exists(Constant::DOC_GENDER, $personalData) ? $personalData[Constant::DOC_GENDER]['value'] : '',
                'birthday' => array_key_exists(Constant::DOC_BIRTHDAY, $personalData) ? date('d.m.Y', strtotime($personalData[Constant::DOC_BIRTHDAY]['value'])) : '',
                'docType' => array_key_exists(Constant::DOC_TYPE, $personalData) ? $personalData[Constant::DOC_TYPE]['docType'] : '',
                'docValue' => array_key_exists(Constant::DOC_TYPE, $personalData) ? $personalData[Constant::DOC_TYPE]['value'] : '',
                'privilegeName' => $ticket->PrivilageName,
                'privilegeDoc' => array_key_exists('privilegeDoc', $personalData) ? $personalData['privilegeDoc']['docType'] : '',
                'privilegeDocValue' => array_key_exists('privilegeDoc', $personalData) ? $personalData['privilegeDoc']['value'] : '',
                'isReturned' => $this->isTicketReturned($ticket->Number)
            ];
        }

        return $tickets;
    }

    /**
     * @return float
     */
    public function getCost():float
    {
        return $this->order_data->getTotalCost();
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getCostString(): string
    {
        return Yii::$app->formatter->asDecimal($this->order_data->getTotalCost(), 2);
    }

    /**
     * @return array
     */
    public function getOrderBundle(): array
    {
        $tickets = $this->getTickets();
        $items = [];
        $counter = 1;
        foreach ($tickets as $ticket) {
            $items[] = [
                'positionId' => $counter++,
                'name' => $ticket['fareName'] . ' билет №' . $ticket['number'],
                'quantity' => [
                    'value' => 1,
                    "measure" =>  "штук"
                ],
                'itemAmount' => $ticket['cost'] * 100,
                'itemCode' => $ticket['number'],
                'agentInterest' => [
                    'interestType' => 'агентская комиссия',
                    'interestValue' => $ticket['tax']
                ],
                'itemPrice' => $ticket['cost'] * 100
            ];
        }

        return [
            'cartItems' => [
                'items' => $items
            ]
        ];
    }

    /**
     * @return int
     */
    public function getPlatform(): int
    {
        return $this->order_data->Trip->Platform;
    }

    /**
     * @return bool
     */
    public function isActual(): bool
    {
        return ($this->status == Constant::ORDER_STATUS_PAID && (strtotime($this->order_data->DepartureTime) > time())) ||
            ($this->status == Constant::ORDER_STATUS_NOT_PAID && ($this->created_at + 60 * 25 > time()));
    }

    /**
     * @param array $params
     * @return OrderRecordInterface[]
     */
    public static function search(array $params): array
    {
        $query = self::find()->orderBy('created_at DESC');

        if (array_key_exists('created_at', $params) && $params['created_at']) {
            $query->andWhere(['ilike', 'to_char(to_timestamp(created_at), \'DD.MM.YYYY\')', $params['created_at']]);
        }

        if (array_key_exists('user_id', $params) && $params['user_id']) {
            $query->andWhere(['user_id' =>  $params['user_id']]);
        }

        return $query->indexBy('index')->all();
    }

    /**
     * @param $id
     * @return OrderRecordInterface|null
     */
    public static function findRecord($id): ?OrderRecordInterface
    {
        return self::findOne($id);
    }
    //====================== /OrderRecordInterface implementation ==============
}
