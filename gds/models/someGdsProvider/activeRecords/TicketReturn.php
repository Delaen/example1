<?php

namespace app\modules\gds\models\someGdsProvider\activeRecords;

use app\modules\gds\components\interfaces\TicketReturnInterface;
use app\modules\gds\helpers\SomeGdsProviderExtractor;
use app\modules\gds\models\someGdsProvider\Constant;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\HttpException;

/**
 * This is the model class for table "some_gds_provider_ticket_returns".
 *
 * @property int $id
 * @property int $order_id
 * @property string $ticket_number
 * @property string $status
 * @property string $return_order_id
 * @property \app\modules\gds\models\someGdsProvider\ReturnOrder $return_order
 * @property int $created_at
 * @property int $updated_at
 * @property boolean $bank_transaction
 * @property string $reason
 * @property boolean $accepted
 *
 * @property Order $order
 */
class TicketReturn extends \yii\db\ActiveRecord implements TicketReturnInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'some_gds_provider_ticket_returns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'ticket_number', 'return_order_id'], 'required'],
            [['order_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['status', 'ticket_number', 'return_order_id', 'reason'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => Constant::TICKET_RETURN_STATUS_NOT_PAID],
            [['status'], 'in', 'range' => Constant::ticketReturnStatuses()],
            [['reason'], 'in', 'range' => Constant::ticketReturnReasons()],
            [['bank_transaction'], 'boolean'],
            [['return_order'], 'safe'],
            [['bank_transaction', 'accepted'], 'default', 'value' => false],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'ticket_number' => 'Ticket Number',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function beforeSave($insert)
    {
        if (!($this->return_order instanceof \app\modules\gds\models\someGdsProvider\ReturnOrder)) {
            throw new HttpException(500, 'Not Order instance');
        }

        $this->return_order = json_encode($this->return_order);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->return_order = SomeGdsProviderExtractor::extractObject(json_decode($this->return_order), \app\modules\gds\models\someGdsProvider\ReturnOrder::class);
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        $this->return_order = SomeGdsProviderExtractor::extractObject(json_decode($this->return_order), \app\modules\gds\models\someGdsProvider\ReturnOrder::class);
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return string
     */
    public function getTicketNumber():string
    {
        return $this->ticket_number;
    }

    /**
     * @return string
     */
    public function getOrderId():string
    {
        return \app\modules\gds\models\TicketReturn::GDS_SOME_PROVIDER . '_' . $this->order_id;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->order->user_id;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->order->user->fullName;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return date('d.m.Y H:i', $this->created_at);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return Constant::ticketReturnStatuses(true)[$this->status];
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->return_order->Amount;
    }

    /**
     * @param array $params
     * @return TicketReturnInterface[]
     */
    public static function search(array $params): array
    {
        $query = self::find();

        if (array_key_exists('user_id', $params) && $params['user_id']) {
            $query->innerJoinWith('order as order');
            $query->andWhere(['order.user_id' => $params['user_id']]);
        }

        return $query->all();
    }
}
