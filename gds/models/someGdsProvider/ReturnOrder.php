<?php
namespace app\modules\gds\models\someGdsProvider;

use app\modules\gds\helpers\SomeGdsProviderExtractor;
use yii\web\HttpException;

class ReturnOrder extends GdsObject
{
    /**
     * @var string
     */
    public $Number;
    /**
     * @var null|string
     */
    public $Date;
    /**
     * @var null|TicketReturn[]
     */
    public $TicketReturns;
    /**
     * @var float
     */
    public $Amount;
    /**
     * @var null|PaymentItem[]
     */
    public $PaymentItems;
    /**
     * @var null|Services
     */
    public $Services;
    /**
     * @var null|LoyaltyCard
     */
    public $LoyaltyCard;
    /**
     * @var null|PaymentDoc
     */
    public $PaymentDoc;
    /**
     * @var null|string
     */
    public $Currency;

    public function __construct(\stdClass $object)
    {
        parent::__construct($object);
        property_exists($object, 'Services') && $this->Services = SomeGdsProviderExtractor::extractObject($object->Services, Services::class);
        property_exists($object, 'LoyaltyCard') && $this->LoyaltyCard = SomeGdsProviderExtractor::extractObject($object->LoyaltyCard, LoyaltyCard::class);
        property_exists($object, 'PaymentDoc') && $this->PaymentDoc = SomeGdsProviderExtractor::extractObject($object->PaymentDoc, PaymentDoc::class);

        if (property_exists($object, 'TicketReturns')) {
            $this->TicketReturns = SomeGdsProviderExtractor::extractObjects($object->TicketReturns, TicketReturn::class);
        }
        if (property_exists($object, 'PaymentItems')) {
            $this->PaymentItems = SomeGdsProviderExtractor::extractObjects($object->PaymentItems, PaymentItem::class);
        }
    }

    /**
     * @param $ticketNumber
     * @return Ticket|null
     * @throws HttpException
     */
    public function getTicket($ticketNumber)
    {
        foreach ($this->getTicketReturns() as $ticketReturn) {
           if (!$ticketReturn->Ticket instanceof Ticket) {
               throw new HttpException(500, 'TicketReturn has no Ticket instance');
           }

           if ($ticketReturn->Ticket->Number == $ticketNumber) {
               return $ticketReturn->Ticket;
           }
        }

        return null;
    }


    /**
     * @return TicketReturn[]
     */
    public function getTicketReturns()
    {
        if (is_array($this->TicketReturns)) {
            return $this->TicketReturns;
        }elseif($this->TicketReturns instanceof TicketReturn) {
            return [$this->TicketReturns];
        }

        return [];
    }
}