<?php

namespace app\modules\gds\models;

use app\modules\gds\components\interfaces\OrderRecordInterface;
use app\modules\gds\models\someGdsProvider\activeRecords\Order as SomeGdsProviderOrder;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\web\HttpException;

abstract class Order
{
    const GDS_SOME_PROVIDER = 'someGdsProvider';

    /**
     * Each element must implement OrderRecordInterface
     */
    private const GDS_CORRELATION = [
        self::GDS_SOME_PROVIDER => \app\modules\gds\models\someGdsProvider\activeRecords\Order::class,
    ];

    /**
     * Возвращает массив всех заказов всех систем (каждый элемент - объект реазиловывающий OrderRecordInterface)
     * @param bool $adminMode
     * @return array
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public static function getOrders($adminMode = true)
    {
        $query = null;
        foreach (self::getOrderClasses() as $gdsName => $class) {
            $subQuery = $class::find()->select(['id', 'user_id', 'created_at']);
            $subQuery->addSelect(new Expression("'" . $gdsName ."' as gds"));

            if (!$adminMode || !Yii::$app->user->can('admin')) {
                $subQuery->where(['user_id' => Yii::$app->user->getId()]);
            }

            !$query && ($query = $subQuery) || $query->union($subQuery->createCommand()->rawSql);
        }

        $rows = Yii::$app->db->createCommand($query->asArray()->createCommand()->rawSql . ' ORDER BY "created_at" DESC')->queryAll();
        return self::getModelsFromArray($rows);
    }

    /**
     * @param string $id
     * @return null|OrderRecordInterface
     * @throws HttpException
     */
    public static function findOrder(string $id): ?OrderRecordInterface
    {
        $exploded = explode('_', $id);

        if (count($exploded) != 2) {
            //throw new HttpException(400, 'Формат id заказа неверен');
            return null;
        }
        $gds = $exploded[0];
        $id = $exploded[1];

        if (array_key_exists($gds, self::GDS_CORRELATION)) {
            return (self::GDS_CORRELATION)[$gds]::findRecord($id);
        }

        return null;
    }

    /**
     * @return SomeGdsProviderOrder[]
     */
    public static function getSomeGdsProviderOrders()
    {
        $query = SomeGdsProviderOrder::find()->with('ticketReturns');
        !Yii::$app->user->can('admin') && $query->where(['user_id' => Yii::$app->user->getId()]);
        return $query->all();
    }

    /**
     * @param int $pageSize
     * @param bool $adminMode
     * @return ArrayDataProvider
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public static function getDataProvider($adminMode = true, int $pageSize = 10)
    {
        $elements = self::getOrders($adminMode);

        $provider = new ArrayDataProvider([
            'allModels' => $elements,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }

    /**
     * @return array, purified
     */
    public static function getOrderClasses()
    {
        $classes = self::GDS_CORRELATION;
        foreach ($classes as $key => $class) {
            if (!in_array(OrderRecordInterface::class, class_implements($class))) {
                unset($classes[$key]);
            }
        }
        return $classes;
    }

    /**
     * @param array $orders
     * @return OrderRecordInterface[]
     */
    private static function getPurifiedOrders(array $orders)
    {
        $purified = [];
        foreach ($orders as $order) {
            if ($order instanceof OrderRecordInterface) {
                $purified[] = $order;
            }
        }

        return $purified;
    }

    private static function getModelsFromArray(array $data)
    {
        $elements = [];
        $ordered = [];
        $models = [];
        foreach ($data as $order => $row) {
            if (!is_array($row)) {
                throw new HttpException(500, 'Нарушен формат данных заказов');
            }

            $elements[$row['gds']][] = $row['id'];
            $ordered[] = ['gds' => $row['gds'], 'id' => $row['id']];
        }

        foreach ($elements as $gds => $ids) {
            $models[$gds] = (self::getOrderClasses()[$gds])::find()->where(['id' => $ids])->indexBy('id')->all();
        }

        $result = [];
        foreach ($ordered as $item) {
            $result[] =  $models[$item['gds']][$item['id']];
        }

        return $result;
    }
}