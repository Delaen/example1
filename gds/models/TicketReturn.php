<?php
namespace app\modules\gds\models;

use app\modules\gds\components\interfaces\TicketReturnInterface;
use app\modules\gds\models\someGdsProvider\activeRecords\TicketReturn as SomeGdsProviderReturn;
use yii\data\ArrayDataProvider;
use yii\web\HttpException;
use Yii;

class TicketReturn
{
    const GDS_SOME_PROVIDER = 'someGdsProvider';

    /**
     * Each element must implement TicketReturnInterface
     * @return array
     */
    private static function getGdsOrderCorrelation():array
    {
        return [
            self::GDS_SOME_PROVIDER => \app\modules\gds\models\someGdsProvider\activeRecords\TicketReturn::class
        ];
    }

    /**
     * @param array $orders
     * @return TicketReturnInterface[]
     */
    private static function getPurified(array $orders)
    {
        $purified = [];
        foreach ($orders as $order) {
            if ($order instanceof TicketReturnInterface) {
                $purified[] = $order;
            }
        }

        return $purified;
    }

    /**
     * Возвращает массив всех заказов всех систем
     * @return array
     */
    public static function getAll()
    {
        $elements = self::getSomeGdsProviderReturns();

        return self::getPurified($elements);
    }

    /**
     * @return SomeGdsProviderReturn[]
     */
    public static function getSomeGdsProviderReturns()
    {
        $query = SomeGdsProviderReturn::find()->with('order');
        !Yii::$app->user->can('admin') && $query->where(['user_id' => Yii::$app->user->getId()]);
        return $query->all();
    }

    /**
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    public static function getDataProvider(int $pageSize = 10)
    {
        $elements = self::getAll();

        $provider = new ArrayDataProvider([
            'allModels' => $elements,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $provider;
    }

    /**
     * @return array, purified
     */
    public static function getReturnClasses()
    {
        $classes = self::getGdsOrderCorrelation();
        foreach ($classes as $key => $class) {
            if (!in_array(TicketReturnInterface::class, class_implements($class))) {
                unset($classes[$key]);
            }
        }
        return $classes;
    }
}