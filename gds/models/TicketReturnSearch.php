<?php
namespace app\modules\gds\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;

class TicketReturnSearch extends Model
{
    public $created_at;
    public $user_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'user_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param int $pageSize
     * @return ArrayDataProvider
     */
    public function search($params, int $pageSize = 20): ArrayDataProvider
    {
        $this->load($params, '');

        $models = [];
        foreach (TicketReturn::getReturnClasses() as $returnClass) {
            $models = array_merge($models, $returnClass::search($this->toArray()));
        }

        return new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);
    }
}