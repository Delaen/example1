<?php

namespace app\modules\gds;

use Yii;
use yii\log\FileTarget;
use yii\web\Application;
use yii\web\Response;

/**
 * someGdsProvider module definition class
 */
class GdsModule extends \yii\base\Module
{
    const ID = 'gds';
    /**
     * {@inheritdoc}
     */
    public $id = self::ID;
    public $controllerNamespace = 'app\modules\gds\controllers';
    public $defaultRoute = 'main';
    public $layout = 'main';
    public $controllerMap = [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@app/modules/gds/console/migrations',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        date_default_timezone_set('Asia/Novosibirsk');
        parent::init();

        if (Yii::$app instanceof Application) {
            $this->formatResponseBeforeSend();
            !isset(Yii::$app->request->parsers['application/json']) && Yii::$app->request->parsers['application/json'] = 'yii\web\JsonParser';
        } elseif (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'app\modules\gds\console\controllers';
        }

        $this->addLogTargets();
    }

    /**
     * Formatting response on before send event of App
     */
    private function formatResponseBeforeSend()
    {
        Yii::$app->response->on(Response::EVENT_BEFORE_SEND, function ($event) {
            $response = $event->sender;
            if ($response->statusCode === 200) {
                $response->data = [
                    'status' => $response->statusCode,
                    'message' => 'OK',
                    'data' => $response->data
                ];
            } elseif ((int)$response->statusCode >= 400) {
                $response->data = [
                    'status' => $response->statusCode,
                    'message' => isset($response->data['message']) ? $response->data['message'] : '',
                    'data' => null
                ];
            }
        });
    }

    private function addLogTargets()
    {
        $target = new FileTarget([
            'levels' => ['error', 'warning', 'info'],
            'categories' => ['someGdsProvider.*'],
            'exportInterval' => 100,
            'logVars' => []
        ]);

        array_push(Yii::$app->getLog()->targets, $target);
    }
}
