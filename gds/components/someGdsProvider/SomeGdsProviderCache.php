<?php

namespace app\modules\gds\components\someGdsProvider;

use app\modules\gds\components\someGdsProvider\TripFilter;
use app\modules\gds\models\someGdsProvider\activeRecords\BusStop as BusStopModel;
use app\modules\gds\models\someGdsProvider\activeRecords\DepartureDestination;
use app\modules\gds\models\someGdsProvider\Trip;
use aracoool\uuid\Uuid;
use yii\db\Query;
use Yii;

final class SomeGdsProviderCache
{
    /**
     * Supported units of time by calculation expiration date
     */
    const TIME_UNITS = ['m', 'h', 'd'];
    const BUS_STOP_TTL = '7d';
    const TRIP_TTL = '10m';
    const TIME_BEFORE_DEPARTURE = '0';
    /**
     * @var SomeGdsProviderCache
     */
    private static $instance;
    /**
     * @var SomeGdsProviderService
     */
    private $service;

    /**
     * SomeGdsProviderCache constructor.
     * @throws \SoapFault
     */
    private function __construct()
    {
        $this->service = SomeGdsProviderService::getInstance();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * @return SomeGdsProviderCache
     * @throws \SoapFault
     */
    public static function getInstance(): SomeGdsProviderCache
    {
        if (static::$instance === null) {
            static::$instance = new self();
        }

        return self::$instance;
    }

    public static function tableName()
    {
        return '{{%some_gds_provider_cache}}';
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getDepartures()
    {
        $cache = $this->getCache('departures');
        if ($cache) {
            return $cache;
        }

        $result = $this->queryDepartures();
        if (empty($result)) {
            return [];
        }

        $this->saveCache('departures', $result, self::BUS_STOP_TTL, $cache === null);
        return $result;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getLocalities()
    {
        $cache = $this->getCache('localities');
        if ($cache) {
            return $cache;
        }

        $result = $this->queryLocalities();
        if (empty($result)) {
            return [];
        }

        $this->saveCache('localities', $result, self::BUS_STOP_TTL, $cache === null);
        return $result;
    }

    /**
     * @param string $locality
     * @return array|bool|mixed|null
     * @throws \yii\db\Exception
     */
    public function getBusStopsByLocality(string $locality)
    {
        $hash = md5('locality_' . $locality);
        $cache = $this->getCache($hash);
        if ($cache) {
            return $cache;
        }

        $result = $this->queryBusStopsByLocality($locality);
        if (empty($result)) {
            return [];
        }

        $this->saveCache($hash, $result, self::BUS_STOP_TTL, $cache === null);
        return $result;
    }

    /**
     * @return array|bool|mixed|null
     * @throws \yii\db\Exception
     */
    public function getDefaultDeparture()
    {
        $cache = $this->getCache('defaultDeparture');
        if ($cache) {
            return $cache;
        }

        $result = $this->queryDefaultDeparture();
        if (empty($result)) {
            return [];
        }

        $this->saveCache('defaultDeparture', $result, self::BUS_STOP_TTL, $cache === null);
        return $result;
    }

    /**
     * @param string $departureId
     * @return array
     * @throws \yii\db\Exception
     */
    public function getDestinations(string $departureId)
    {
        if (Uuid::isValid($departureId)) {
            $hash = md5('destinations' . $departureId);
            $cache = $this->getCache($hash);
            if ($cache) {
                return $cache;
            }

            $result = $this->queryDestinations($departureId);
            if (empty($result)) {
                return [];
            }

            $this->saveCache($hash, $result, self::BUS_STOP_TTL, $cache === null);
            return $result;
        }

        return [];
    }

    /**
     * @param string $departureId
     * @param string $destinationId
     * @param string $tripDate
     * @param array $filterParams
     * @return \app\modules\gds\models\someGdsProvider\Trip|\app\modules\gds\models\someGdsProvider\Trip[]
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function getTrips(string $departureId, string $destinationId, string $tripDate, array $filterParams)
    {
        if (Uuid::isValid($departureId) && Uuid::isValid($destinationId)) {
            $personsNumber = array_key_exists('personsNumber', $filterParams) ? $filterParams['personsNumber'] : '';
            $comment = array_key_exists('comment', $filterParams) ? $filterParams['comment'] : '';
            $hash = md5('trips' . $departureId . $destinationId . $tripDate . $personsNumber . $comment);
            $cache = $this->getCache($hash);
            if ($cache) {
                return $cache;
            }

            $result = $this->queryTrips($departureId, $destinationId, $tripDate);
            if (!$result) {
                return [];
            }

            $filter = new TripFilter($result);
            $personsNumber && $filter->filterByPersonsNumber($personsNumber);
            $comment && $filter->filterByComment($comment);
            $result = $filter->filterByNullPrice()->getTrips();
            $this->saveCache($hash, $result, self::TRIP_TTL, $cache === null);
            return $result;
        }

        return [];
    }

    /**
     * @param string $tripId
     * @param string $departureId
     * @return \app\modules\gds\models\someGdsProvider\Trip|null
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function getTrip(string $tripId, string $departureId)
    {
        if (Uuid::isValid($tripId) && Uuid::isValid($departureId)) {
            $hash = md5('trip' . $tripId . $departureId);
            $cache = $this->getCache($hash);
            if ($cache) {
                return $cache;
            }

            $result = $this->queryTrip($tripId, $departureId);
            if (!$result) {
                return null;
            }

            $this->saveCache($hash, $result, self::TRIP_TTL, $cache === null);
            return $result;
        }

        return null;
    }

    /**
     * @param string $tripId
     * @param string $departureId
     * @param string $destinationId
     * @return SaleSession|null
     * @throws \SoapFault
     * @throws \yii\web\HttpException
     */
    public function startSaleSession(string $tripId, string $departureId, string $destinationId): ?SaleSession
    {
        if (Uuid::isValid($tripId) && Uuid::isValid($departureId) && Uuid::isValid($destinationId)) {
            $route = DepartureDestination::find()->with('departure', 'destination')
                ->where(['id_departure' => $departureId, 'id_destination' => $destinationId])
                ->one();
            if ($route) {
                $saleSession = SaleSession::getInstance();
                $saleSession->start($tripId, $route->departure->gds_id, $route->destination->gds_id);
                return $saleSession;
            }
        }

        return null;
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function updateRoutes()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->updateBusStops() && $this->updateDestinations()) {
            $this->flush();
            $transaction->commit();
            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * Flushes all caches
     */
    public function flush()
    {
        Yii::$app->db->createCommand()->truncateTable(self::tableName())->execute();
    }

    /**
     * Flushes deprecated caches
     * @throws \yii\db\Exception
     */
    public function flushDeprecated()
    {
        Yii::$app->db->createCommand()->delete(self::tableName(), ['<', 'expiration', time()])->execute();
    }

    /**
     * @param string $hash
     * @return bool|mixed|null
     */
    private function getCache(string $hash)
    {
        $query = new Query();
        $result = $query->select(['data', 'expire_at'])->from(self::tableName())->where(['hash' => $hash])->one();

        if (is_array($result)) {
            if ($result && $result['expire_at'] > time()) {
                return unserialize(base64_decode($result['data']));
            }
            return false;
        }

        return null;
    }

    /**
     * @param string $hash
     * @param array|object $data
     * @param string $ttl
     * @param bool $insert
     * @throws \yii\db\Exception
     */
    private function saveCache(string $hash, $data, string $ttl, bool $insert = true)
    {
        $expire_at = $this->calculateExpiration($ttl);

        if ($expire_at !== null && (is_array($data) || is_object($data))) {
            $command = Yii::$app->db->createCommand();
            $params = ['hash' => $hash, 'data' => base64_encode(serialize($data)), 'expire_at' => $expire_at];

            if ($insert) {
                $command->insert(self::tableName(), $params)->execute();
            } else {
                $command->update(self::tableName(), $params, ['hash' => $hash])->execute();
            }
        }
    }

    /**
     * @return array
     */
    private function queryDepartures(): array
    {
        return BusStopModel::find()->select(['id', 'name', 'region', 'locality'])
            ->where(['automated' => true, 'has_destinations' => true])->indexBy('id')->all();
    }

    /**
     * @return array
     */
    private function queryLocalities()
    {
        return BusStopModel::find()->select(['locality'])->distinct()
            ->where(['IS NOT', 'locality', null])->orderBy('locality')->column();
    }

    /**
     * @param string $locality
     * @return array
     */
    private function queryBusStopsByLocality(string $locality)
    {
        return BusStopModel::find()->select(['id', 'name', 'region', 'locality'])
            ->where(['locality' => $locality])->orderBy('name')->indexBy('id')->all();
    }

    /**
     * @return array
     */
    private function queryDefaultDeparture()
    {
        $query = BusStopModel::find()->select(['name', 'id'])
            ->where(['automated' => true, 'has_destinations' => true])
            ->indexBy('id')->limit('1');
        $busStop = (clone $query)->andWhere(['ilike', 'name', 'Новосибирск'])->column();

        if (empty($busStop)) {
            $busStop = (clone $query)->andWhere(['ilike', 'locality', 'Новосибирск'])->column();
        }
        if (empty($busStop)) {
            $busStop = (clone $query)->andWhere(['ilike', 'region', 'Новосибирск'])->column();
        }
        if (empty($busStop)) {
            $busStop = $query->column();
        }

        return $busStop;
    }

    /**
     * @param string $departureId (!!! must be in UUID format !!!)
     * @return array
     */
    private function queryDestinations(string $departureId): array
    {
        return BusStopModel::find()->select(['id', 'name', 'region', 'locality'])->asArray()
            ->innerJoin(DepartureDestination::tableName() . ' as dd', BusStopModel::tableName() . '.id=dd.id_destination')
            ->where(['dd.id_departure' => $departureId])
            ->indexBy('id')
            ->all();
    }

    /**
     * @param string $departureId
     * @param string $destinationId
     * @param string $tripDate
     * @return \app\modules\gds\models\someGdsProvider\Trip[]
     * @throws \yii\web\HttpException
     */
    private function queryTrips(string $departureId, string $destinationId, string $tripDate): array
    {
        $endReserveTime = time() - $this->calculateTimestamp(self::TIME_BEFORE_DEPARTURE);

        $route = DepartureDestination::find()->with('departure', 'destination')
            ->where(['id_departure' => $departureId, 'id_destination' => $destinationId])
            ->one();
        if ($route) {
            $result = $this->service->getTrips($route->departure->gds_id, $route->destination->gds_id, $tripDate);
            if (is_array($result)) {
                foreach ($result as $key => $trip) {
                    $date = new \DateTime($trip->DepartureTime, new \DateTimeZone('Asia/Novosibirsk'));
                    if ($date->getTimestamp() < $endReserveTime) {
                        unset($result[$key]);
                    }
                }
                return $result;
            } elseif ($result instanceof Trip) {
                $date = new \DateTime($result->DepartureTime);
                if ($date->getTimestamp() < $endReserveTime) {
                    return [];
                }
                return [$result];
            }
        }

        return [];
    }

    /**
     * @param string $tripId
     * @param string $departureId
     * @return \app\modules\gds\models\someGdsProvider\Trip|null
     * @throws \yii\web\HttpException
     */
    private function queryTrip(string $tripId, string $departureId)
    {
        $departure = BusStopModel::findOne($departureId);
        if ($departure) {
            return $this->service->getTrip($tripId, $departure->gds_id);
        }

        return null;
    }

    /**
     * @return bool
     * @throws \yii\web\HttpException
     */
    private function updateBusStops()
    {
        $busStops = $this->service->getBusStops();
        $addBusStops = $this->service->getBusStops(SomeGdsProviderService::WSDL_SCHEDULE_PORT_PATH);

        if (is_array($busStops) && !empty($busStops)) {
            if (is_array($addBusStops) && !empty($addBusStops)) {
                $addBusStopsById = [];
                foreach ($addBusStops as $key => $busStop) {
                    $busStop->Id && $addBusStopsById[$busStop->Id] = $busStop;
                }

                foreach ($busStops as $busStop) {
                    if (array_key_exists($busStop->Id, $addBusStopsById)) {
                        $busStop->mergeAttributes($addBusStopsById[$busStop->Id]);
                    }
                }
            }

            BusStopModel::deleteAll();
            foreach ($busStops as $busStop) {
                $busStopModel = new BusStopModel();
                $busStopModel->loadFromGds($busStop);
                $busStopModel->save();
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws \yii\web\HttpException
     */
    private function updateDestinations()
    {
        $busStops = BusStopModel::find()->select(['id', 'gds_id', 'has_destinations', 'automated', 'name'])->asArray()->indexBy('gds_id')->all();
        DepartureDestination::deleteAll();
        foreach ($busStops as $gdsId => $busStop) {
            if ($busStop['has_destinations'] && $busStop['automated']) {

                //При ошибке пишем в лог и переходим на след. элемент
                try {
                    $destinations = $this->service->getDestinations($gdsId, '');
                } catch (\Exception $exception) {
                    $errorMsg = 'Ошибка поиска направлений по остановке ' . $busStop['name'];
                    $errorMsg .= ' (gdsId - ' . $busStop['gds_id'] . ')';
                    Yii::error($errorMsg, 'someGdsProvider.busStop.destinations');
                    continue;
                }

                if (is_array($destinations)) {
                    foreach ($destinations as $destination) {
                        if (array_key_exists($destination->Id, $busStops)) {
                            (new DepartureDestination(['id_departure' => $busStop['id'], 'id_destination' => $busStops[$destination->Id]['id']]))->save();
                        }
                    }
                } elseif ($destinations instanceof \stdClass) {
                    if (array_key_exists($destinations->Id, $busStops)) {
                        (new DepartureDestination(['id_departure' => $busStop['id'], 'id_destination' => $busStops[$destinations->Id]['id']]))->save();
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param string $ttl
     * @return int|null timestamp of expiration date or null if passed wrong ttl format
     */
    private function calculateExpiration(string $ttl)
    {
        $expiration = $this->calculateTimestamp($ttl);
        return $expiration ? (time() + $expiration) : null;
    }

    private function calculateTimestamp($timeString)
    {
        $timeUnit = $timeString[strlen($timeString) - 1];
        $seconds = (int)$timeString;

        if ($seconds !== 0) {
            $result = $seconds;
            switch ($timeUnit) {
                case 'd':
                    $result *= 24;
                case 'h':
                    $result *= 60;
                case 'm':
                    $result *= 60;
            }

            return $result;
        }

        return 0;
    }
}
