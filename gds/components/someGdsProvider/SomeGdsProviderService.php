<?php
namespace app\modules\gds\components\someGdsProvider;

use app\modules\gds\models\someGdsProvider\Bus;
use app\modules\gds\models\someGdsProvider\BusSeat;
use app\modules\gds\models\someGdsProvider\BusStop;
use app\modules\gds\models\someGdsProvider\ChequeSettings;
use app\modules\gds\models\someGdsProvider\Constant;
use app\modules\gds\helpers\SomeGdsProviderExtractor;
use app\modules\gds\models\someGdsProvider\Fare;
use app\modules\gds\models\someGdsProvider\GdsObject;
use app\modules\gds\models\someGdsProvider\Order;
use app\modules\gds\models\someGdsProvider\PaymentItem;
use app\modules\gds\models\someGdsProvider\Privilege;
use app\modules\gds\models\someGdsProvider\ReturnOrder;
use app\modules\gds\models\someGdsProvider\Ticket;
use app\modules\gds\models\someGdsProvider\TicketSeat;
use app\modules\gds\models\someGdsProvider\Trip;
use yii\validators\DateValidator;
use yii\web\HttpException;

final class SomeGdsProviderService
{
    const WSDL_PATH = 'http://some_url/?wsdl';
    const WSDL_SCHEDULE_PORT_PATH = 'http://some_url/?wsdl';
    const LOGIN = 'user';
    const PASSWORD = 'user';
    /**
     * @var SomeGdsProviderService
     */
    private static $instance;
    /**
     * @var \SoapClient
     */
    private $soapClient;

    /**
     * SomeGdsProviderService constructor.
     * @throws HttpException
     */
    private function __construct()
    {
        try {
            $this->soapClient = new \SoapClient(self::WSDL_PATH, [
                'soap_version' => SOAP_1_2,
                'cache_wsdl'    => WSDL_CACHE_NONE,
                'login' => self::LOGIN,
                'password' => self::PASSWORD,
                'trace' => true
            ]);
        }catch (\Exception $e) {
            throw new HttpException(400, 'Сервис продажи билетов недоступен');
        }
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * @return SomeGdsProviderService
     * @throws HttpException
     * @return self
     */
    public static function getInstance(): SomeGdsProviderService
    {
        if (static::$instance === null) {
            static::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws HttpException
     */
    public function __call($name, $arguments)
    {
        try {
            return $this->soapClient->$name($arguments[0]);
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось выполнить запрос');
        }
    }

    /**
     * @param null $wsdlPath
     * @return BusStop[]|BusStop
     * @throws HttpException
     */
    public function getBusStops($wsdlPath = null)
    {
        $wsdlPath && $this->soapClient->SoapClient(self::WSDL_SCHEDULE_PORT_PATH);
        try {
            $response = $this->soapClient->getBusStops()->return;

            //Возвращаем искомые настройки - устанавливаем wsdl по умолчанию
            $wsdlPath && $this->soapClient->SoapClient(self::WSDL_PATH);
            return property_exists($response, 'Elements') ? $this->elementsToClassObjects($response->Elements, BusStop::class) : [];
        }catch (\Exception $e) {
            throw new HttpException(400, 'Не удалось получить список остановок');
        }
    }

    /**
     * @param string $busStopId
     * @param string $substring
     * @return BusStop|BusStop[]
     * @throws HttpException
     */
    public function getDestinations(string $busStopId, $substring = '')
    {
        $getDestinations = new \stdClass();
        $getDestinations->Substring = '';
        $getDestinations->Departure = $busStopId;

        try {
            $response = $this->soapClient->getDestinations($getDestinations)->return;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Пункты назначения не найдены');
        }

        return property_exists($response, 'Elements') ? $this->elementsToClassObjects($response->Elements, BusStop::class) : [];
    }

    /**
     * @param string $departureId
     * @param string $destinationId
     * @param string $tripsDate
     * @return Trip[]|Trip
     * @throws HttpException
     */
    public function getTrips(string $departureId, string $destinationId, string $tripsDate)
    {
        $dateValidator = new DateValidator(['format' => 'php:Y-m-d']);
        if (!$dateValidator->validate($tripsDate)) {
            throw new HttpException(400, 'Некорректный формат даты');
        }

        $params = new \stdClass();
        $params->Departure = $departureId;
        $params->Destination = $destinationId;
        $params->TripsDate = $tripsDate;

        try {
            $response = $this->soapClient->getTrips($params)->return;
            $result = property_exists($response, 'Elements') ? $this->elementsToClassObjects($response->Elements, Trip::class) : [];
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Рейсы не найдены');
        }

        return $result;
    }

    /**
     * @param string $tripId
     * @param string $busStopId
     * @return Trip|null
     * @throws HttpException
     */
    public function getTrip(string $tripId, string $busStopId)
    {
        $params = new \stdClass();
        $params->TripId = $tripId;
        $params->BusStop = $busStopId;

        try {
            $response = $this->soapClient->GetTrip($params);
            return $this->elementToClassObject($response->return, Trip::class);
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось получить информацию по рейсу');
        }catch (\Exception $e) {
            throw new HttpException(500, 'Неизвестная ошибка');
        }
    }

    /**
     * @param string $tripId
     * @param string $departureId
     * @param string $destinationId
     * @param string $orderId
     * @return GdsObject|mixed|null
     * @throws HttpException
     */
    public function startSaleSession(string $tripId, string $departureId, string $destinationId, $orderId = '')
    {
        $params = new \stdClass();
        $params->TripId = $tripId;
        $params->Departure = $departureId;
        $params->Destination = $destinationId;
        $params->OrderId = $orderId;

        try {
            $response = $this->soapClient->StartSaleSession($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, Order::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось начать продажу билетов');
        }
    }

    /**
     * @param $orderId
     * @param Fare[] $ticketSeats
     * @return Order|null
     * @throws HttpException
     */
    public function addTickets(string $orderId, array $ticketSeats): ?Order
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;
        $seats = [];

        foreach ($ticketSeats as $key => $seat) {
            if ($seat instanceof TicketSeat) {
                $seats[] = SomeGdsProviderExtractor::restoreObject($seat);
            }
        }
        $params->TicketSeats = $seats;

        try {
            $response = $this->soapClient->AddTickets($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, Order::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось добавить билеты');
        }
    }

    /**
     * @param $orderId
     * @param array $ticketSeats
     * @return Order|null
     * @throws HttpException
     */
    public function deleteTickets(string $orderId, array $ticketSeats): ?Order
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;
        $seats = [];

        foreach ($ticketSeats as $key => $seat) {
            if ($seat instanceof TicketSeat) {
                $seats[] = SomeGdsProviderExtractor::restoreObject($seat);
            }
        }
        $params->TicketSeats = $seats;

        try {
            $response = $this->soapClient->DelTickets($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, Order::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось удалить билеты');
        }
    }

    /**
     * @param string $orderId
     * @param array $tickets
     * @return Order|null
     * @throws HttpException
     */
    public function setTicketsData(string $orderId, array $tickets)
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;

        $ticketsParam = [];

        foreach ($tickets as $key => $ticket) {
            if ($ticket instanceof Ticket) {
                $ticketsParam[] = SomeGdsProviderExtractor::restoreObject($ticket);
            }
        }
        $params->Tickets = $ticketsParam;
        try {
            $response = $this->soapClient->SetTicketData($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, Order::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось указать данные для билетов');
        }
    }

    /**
     * @param string $orderId
     * @return Order|null
     * @throws HttpException
     */
    public function reserveOrder(string $orderId)
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;

        try {
            $response = $this->soapClient->ReserveOrder($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, Order::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось зарезервировать билеты');
        }
    }

    /**
     * @param string $orderId
     * @param string $ticketNumber
     * @return Privilege|Privilege[]
     * @throws HttpException
     */
    public function getPrivileges(string $orderId, string $ticketNumber)
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;
        $params->TicketNumber = $ticketNumber;

        try {
            $response = $this->soapClient->GetAvailablePrivileges($params);
            return property_exists($response->return, 'Elements') ? $this->elementsToClassObjects($response->return->Elements, Privilege::class) : [];
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Льготы не найдены');
        }
    }

    public function setPrivilege(string $orderId, string $ticketNumber, string $privilegeName)
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;
        $params->TicketNumber = $ticketNumber;
        $params->Privilege = $privilegeName;

        try {
            $response = $this->soapClient->SetPrivilege($params);
            return property_exists($response, 'return') ? $this->elementToClassObject($response->return, Ticket::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось установить льготу');
        }
    }

    /**
     * @param string $tripId
     * @param string $departureId
     * @param string $destinationId
     * @param string $orderId
     * @return array
     * @throws HttpException
     */
    public function getOccupiedSeats(string $tripId, string $departureId, string $destinationId, string $orderId)
    {
        $params = new \stdClass();
        $tripId && $params->TripId = $tripId;
        $params->Departure = $departureId;
        $destinationId && $params->Destination = $destinationId;
        $params->OrderId = $orderId;

        try {
            $response = $this->soapClient->GetOccupiedSeats($params);
            $seats = property_exists($response->return, 'Elements') ? $this->elementsToClassObjects($response->return->Elements, BusSeat::class) : [];
            $bus = property_exists($response, 'Bus') ? $this->elementToClassObject($response->Bus, Bus::class) : null;
            return compact('seats', 'bus');
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Места не найдены');
        }
    }

    /**
     * @param string $orderId
     * @param array $paymentItems
     * @param ChequeSettings $chequeSettings
     * @return GdsObject|mixed|null
     * @throws HttpException
     */
    public function payment(string $orderId, array $paymentItems, ChequeSettings $chequeSettings)
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;

        $items = [];
        foreach ($paymentItems as $item) {
            if ($item instanceof PaymentItem) {
                $items[] = SomeGdsProviderExtractor::restoreObject($item);
            }
        }
        $params->PaymentItems = $items;
        $params->ChequeSettings = $chequeSettings;

        try {
            $response = $this->soapClient->Payment($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, Order::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось провести оплату');
        }
    }

    /**
     * @param string $orderId
     * @param array|null $ticketSeats
     * @param bool|null $services
     * @param array|null $paymentItems
     * @return array
     * @throws HttpException
     */
    public function cancelPayment(string $orderId, array $ticketSeats = null, bool $services = null, array $paymentItems = null)
    {
        $params = new \stdClass();
        $params->OrderId = $orderId;

        if (is_array($ticketSeats) && !empty($ticketSeats)) {
            $seats = [];
            foreach ($ticketSeats as $key => $seat) {
                if ($seat instanceof TicketSeat) {
                    $seats[] = SomeGdsProviderExtractor::restoreObject($seat);
                }
            }
            $params->TicketSeats = $seats;
        }

        if (is_array($paymentItems) && !empty($paymentItems)) {
            $items = [];
            foreach ($paymentItems as $key => $item) {
                if ($item instanceof PaymentItem) {
                    $items[] = SomeGdsProviderExtractor::restoreObject($item);
                }
            }
            $params->PaymentItems = $items;
        }

        $services !== null && $params->Services = $services;

        try {
            $response = $this->soapClient->CancelPayment($params);
            $result = [];
            if ($response->return === true) {
                $result['success'] = true;
                $result['ticketSeats'] = (property_exists($response, 'TicketSeats') && is_array($response->TicketSeats)) ? $this->elementsToClassObjects($response->TicketSeats, TicketSeat::class) : [];
                return $result;
            }

            return ['success' => false, 'ticketSeats' => []];
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось отменить оплату');
        }
    }

    /**
     * @param string $ticketNumber
     * @param string $departureId
     * @param string|null $returnOrderId
     * @return GdsObject|mixed|null
     * @throws HttpException
     */
    public function addTicketReturn(string $ticketNumber, string $departureId, string $returnOrderId = null)
    {
        $params = new \stdClass();
        $params->TicketNumber = $ticketNumber;
        $params->Departure = $departureId;
        ($returnOrderId !== null) && $params->ReturnOrderId = $returnOrderId;

        try {
            $response = $this->soapClient->AddTicketReturn($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, ReturnOrder::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось создать заявку на возврат');
        }
    }

    /**
     * @param string $returnOrderId
     * @param string $ticketNumber
     * @return GdsObject|mixed|null
     * @throws HttpException
     */
    public function deleteTicketReturn(string $returnOrderId, string $ticketNumber)
    {
        $params = new \stdClass();
        $params->ReturnOrderId = $returnOrderId;
        $params->TicketNumber = $ticketNumber;

        try {
            $response = $this->soapClient->DelTicketReturn($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, ReturnOrder::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось удалить билет из заявки на возврат');
        }
    }

    /**
     * @param string $returnOrderId
     * @param ChequeSettings $chequeSettings
     * @return GdsObject|mixed|null
     * @throws HttpException
     */
    public function returnPayment(string $returnOrderId, ChequeSettings $chequeSettings)
    {
        $params = new \stdClass();
        $params->ReturnOrderId = $returnOrderId;
        $params->TerminalId = null;
        $params->PaymentItems = null;
        $params->ChequeSettings = $chequeSettings;

        try {
            $response = $this->soapClient->ReturnPayment($params);
            return is_object($response->return) ? $this->elementToClassObject($response->return, ReturnOrder::class) : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось провести возврат');
        }
    }

    /**
     * @param string $returnOrderId
     * @return bool|null
     * @throws HttpException
     */
    public function cancelReturnPayment(string $returnOrderId)
    {
        $params = new \stdClass();
        $params->ReturnOrderId = $returnOrderId;
        $params->TicketNums = null;
        $params->Services = null;
        $params->PaymentItems = null;

        try {
            $response = $this->soapClient->CancelReturnPayment($params);
            return is_object($response) ? $response->return : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось отменить возврат');
        }
    }

    public function getTicketStatus(string $departureId, string $ticketId, string $vendorId = null)
    {
        $params = new \stdClass();
        $params->DeprtureId = $departureId;
        $params->TicketId = $ticketId;
        $params->VendorId = $vendorId;

        try {
            $response = $this->soapClient->GetTicketStatus($params);
            return is_object($response) ? $response->return : null;
        }catch (\SoapFault $e) {
            throw new HttpException(400, 'Не удалось получить статус');
        }
    }

    /**
     * @param \stdClass $obj
     * @param $className
     * @return mixed
     */
    private function elementToClassObject(\stdClass $obj, string $className) : GdsObject
    {
        return SomeGdsProviderExtractor::extractObject($obj, $className);
    }

    /**
     * @param \stdClass[]|\stdClass $elements
     * @param $className
     * @return array|GdsObject
     */
    private function elementsToClassObjects($elements, string $className)
    {
        return SomeGdsProviderExtractor::extractObjects($elements, $className);
    }

}
