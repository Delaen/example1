<?php
namespace app\modules\gds\components\someGdsProvider\jobs;

use app\helpers\PaymentHelper;
use app\modules\gds\components\someGdsProvider\SomeGdsProviderService;
use app\modules\gds\models\someGdsProvider\activeRecords\TicketReturn;
use app\modules\gds\models\someGdsProvider\ChequeSettings;
use app\modules\gds\models\someGdsProvider\Constant;
use app\modules\gds\models\someGdsProvider\ReturnOrder;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\web\HttpException;

class TicketReturnJob extends BaseObject implements JobInterface
{
    /**
     * @var string
     */
    public $ticketReturnId;
    /**
     * @var TicketReturn
     */
    private $ticketReturn;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     * @throws HttpException
     * @throws \ReflectionException
     * @throws \SoapFault
     * @throws \yii\db\Exception
     */
    public function execute($queue)
    {
        $this->ticketReturn = TicketReturn::findOne($this->ticketReturnId);
        if ($this->ticketReturn && ($this->ticketReturn->status === Constant::TICKET_RETURN_STATUS_NOT_PAID)) {
            if (!$this->ticketReturn->bank_transaction) {
                $this->requestBank();
            }
            $this->requestSomeGdsProvider();
        } else {
            throw new HttpException(400, 'Ticket return not found');
        }
    }

    /**
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    private function requestBank()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $this->ticketReturn->bank_transaction = true;

        if (!$this->ticketReturn->save() ||
            PaymentHelper::createReturnOrder($this->ticketReturn->order_id, $this->ticketReturn->order->user_id, $this->ticketReturn->amount)) {
            throw new HttpException(400, 'Bank request failed');
        }

        $transaction->commit();
    }

    /**
     * @throws HttpException
     * @throws \ReflectionException
     * @throws \SoapFault
     * @throws \yii\db\Exception
     */
    private function requestSomeGdsProvider()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $this->ticketReturn->status = Constant::TICKET_RETURN_STATUS_PAID;

        if (!$this->ticketReturn->save()) {
            throw new HttpException(400, 'Return failed');
        }

        $service = SomeGdsProviderService::getInstance();
        $chequeSettings = ChequeSettings::createObject();
        $chequeSettings->ChequeWidth = Constant::CHEQUE_WIDTH;
        $result = $service->returnPayment($this->ticketReturn->return_order_id, $chequeSettings);

        if (!($result instanceof ReturnOrder)) {
            throw new HttpException(400, 'SomeGdsProvider return failed');
        }

        $transaction->commit();
    }
}