<?php

namespace app\modules\gds\components\someGdsProvider;

use app\helpers\Notifier;
use app\helpers\PaymentHelper;
use app\modules\gds\models\someGdsProvider\activeRecords\TicketReturn;
use app\modules\gds\models\someGdsProvider\ChequeSettings;
use app\modules\gds\models\someGdsProvider\Constant;
use app\modules\gds\models\someGdsProvider\ReturnOrder;
use yii\web\HttpException;

final class ReturnInspector
{
    /**
     * @var ReturnInspector
     */
    private static $instance;
    /**
     * Checking return in this moment
     * @var TicketReturn
     */
    private $ticketReturn;

    /**
     * ReturnInspector constructor.
     */
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * @return ReturnInspector
     */
    public static function getInstance(): ReturnInspector
    {
        if (static::$instance === null) {
            static::$instance = new self();
        }

        return self::$instance;
    }

    public function check()
    {
        $returns = TicketReturn::findAll(['status' => Constant::TICKET_RETURN_STATUS_NOT_PAID, 'accepted' => true]);

        foreach ($returns as $return) {
            $this->checkReturnItem($return);
        }
    }

    private function checkReturnItem(TicketReturn $ticketReturn)
    {
        $this->ticketReturn = $ticketReturn;
        if (!$this->ticketReturn->bank_transaction) {
            $this->requestBank();
        }
        $this->requestSomeGdsProvider();
    }

    /**
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    private function requestBank()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $this->ticketReturn->bank_transaction = true;

        if (!$this->ticketReturn->save() ||
            PaymentHelper::createReturnOrder($this->ticketReturn->order_id, $this->ticketReturn->order->user_id, $this->ticketReturn->amount)) {
            return false;
        }

        $transaction->commit();
        return true;
    }

    /**
     * @throws HttpException
     * @throws \ReflectionException
     * @throws \SoapFault
     * @throws \yii\db\Exception
     */
    private function requestSomeGdsProvider()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $this->ticketReturn->status = Constant::TICKET_RETURN_STATUS_PAID;

        if (!$this->ticketReturn->save()) {
            return false;
        }

        $service = SomeGdsProviderService::getInstance();
        $chequeSettings = ChequeSettings::createObject();
        $chequeSettings->ChequeWidth = Constant::CHEQUE_WIDTH;
        $result = $service->returnPayment($this->ticketReturn->return_order_id, $chequeSettings);

        if (!($result instanceof ReturnOrder)) {
           return false;
        }

        $transaction->commit();

        Notifier::returnOrderAccepted();
        return true;
    }
}