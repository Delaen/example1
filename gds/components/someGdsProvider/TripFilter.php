<?php

namespace app\modules\gds\components\someGdsProvider;

use app\modules\gds\models\someGdsProvider\Trip;
use yii\web\HttpException;

/**
 * Class TripFilter
 * @package app\modules\gds\components\someGdsProvider
 */
class TripFilter
{
    /**
     * @var Trip[]
     */
    private $trips = [];

    /**
     * TripFilter constructor.
     * @param array $trips
     */
    public function __construct(array $trips)
    {
        $result = [];
        foreach ($trips as $trip) {
            ($trip instanceof Trip) && $result[] = $trip;
        }
        $this->trips = $result;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     * @throws HttpException
     */
    public function __call($name, $arguments)
    {
        $method = str_replace('filter', '', $name);
        if (method_exists(self::class, $method)) {
            $this->trips = call_user_func_array(static::class . '::' . $method, array_merge([$this->trips], $arguments));
            return $this;
        }

        throw new HttpException(500, 'Method doesn\'t exist');
    }

    /**
     * @return Trip[]
     */
    public function getTrips()
    {
        return $this->trips;
    }

    /**
     * @param Trip[] $trips
     * @param int $personsNumber
     * @return Trip[] $trips
     */
    static function byPersonsNumber(array $trips, int $personsNumber): array
    {
        $number = abs($personsNumber);
        if ($number != 0) {
            $filtered = [];
            foreach ($trips as $trip) {
                ($trip->FreeSeatsAmount >= $personsNumber) && $filtered[] = $trip;
            }

            return $filtered;
        }

        return $trips;
    }

    /**
     * @param Trip[] $trips
     * @return Trip[]
     */
    static function byNullPrice(array $trips):array
    {
        $filtered = [];
        foreach ($trips as $trip) {
            ($trip->PassengerFareCost > 0) && $filtered[] = $trip;
        }

        return $filtered;
    }

    /**
     * @param Trip[] $trips
     * @param string $substring
     * @return Trip[]
     */
    static function byComment(array $trips, string $substring):array
    {
        $filtered = [];
        foreach ($trips as $trip) {
            mb_stripos($trip->Comment, $substring) && $filtered[] = $trip;
        }

        return $filtered;
    }

}