<?php

namespace app\modules\gds\components\someGdsProvider;

use app\modules\gds\models\someGdsProvider\activeRecords\Order as OrderAR;
use app\modules\gds\models\someGdsProvider\BusSeat;
use app\modules\gds\models\someGdsProvider\BusStop;
use app\modules\gds\models\someGdsProvider\Constant;
use app\modules\gds\models\someGdsProvider\Order;
use app\modules\gds\models\someGdsProvider\Ticket;
use app\modules\gds\models\someGdsProvider\TicketSeat;
use app\modules\gds\models\someGdsProvider\Trip;
use yii\web\HttpException;
use Yii;

final class SaleSession
{
    const SESSION_VAR_NAME = 'saleSessionOrder';
    /**
     * @var SaleSession
     */
    private static $instance;
    /**
     * @var SomeGdsProviderService
     */
    private $service;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var OrderAR модель сохранненного заказа
     * Аттрибут заполняется, только в случаем резерва заказа и создании заказа в БД
     */
    private $orderAR;

    private function __construct()
    {
        $this->service = SomeGdsProviderService::getInstance();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * @throws HttpException
     */
    private function checkOrderExistence()
    {
        if (!$this->order || !($this->order instanceof Order)) {
            throw new HttpException(404, 'Заказ не найден');
        }
    }

    /**
     * @param string $ticketNumber
     * @return bool
     */
    private function checkTicketExistence(string $ticketNumber): bool
    {
        return $this->order->checkTicketExistence($ticketNumber);
    }

    /**
     * @return bool
     */
    private function createOrder(): bool
    {
        $order = new OrderAR([
            'order_data' => $this->order,
            'status' => Constant::ORDER_STATUS_NOT_PAID,
            'user_id' => Yii::$app->user->getId()
        ]);

        $result = $order->save();
        $result && $this->orderAR = $order;
        return $result;
    }

    /**
     * Saves order to session
     */
    private function save()
    {
        Yii::$app->session->set(self::SESSION_VAR_NAME, base64_encode(serialize($this->order)));
    }

    /**
     * Gets order from session
     */
    private function wakeUp()
    {
        $this->order = Yii::$app->session->has(self::SESSION_VAR_NAME) ? unserialize(base64_decode(Yii::$app->session->get(self::SESSION_VAR_NAME))) : null;
    }

    /**
     * @param string $tripId
     * @param string $departureId
     * @param string $destinationId
     * @throws HttpException
     * @throws \SoapFault
     */
    public function start(string $tripId, string $departureId, string $destinationId)
    {
        $this->order = $this->service->startSaleSession($tripId, $departureId, $destinationId);
        $seats = $this->getOccupiedSeats();
        $this->order->OccupiedSeats = $seats;
        $this->order->Trip->FreeSeatsAmount = $this->order->Trip->Bus->SeatCapacity - (($seats instanceof BusSeat) ? 1 : (is_array($seats) ? count($seats) : 0));
        $this->save();
    }

    /**
     * @return bool
     */
    public function restore()
    {
        $this->wakeUp();
        return $this->order ? true : false;
    }

    /**
     * Unsets session variable "saleSessionOrder"
     */
    public function destroy()
    {
        Yii::$app->session->remove(self::SESSION_VAR_NAME);
    }

    /**
     * @return Ticket[]|Ticket|null
     */
    public function getTickets()
    {
        try {
            return $this->order->Tickets ?: [];
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @return Order|null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return OrderAR|null
     */
    public function getOrderAR(): ?OrderAR
    {
        return $this->orderAR;
    }

    /**
     * @param array $seats
     * @return bool
     * @throws HttpException
     */
    public function addTickets(array $seats)
    {
        $this->checkOrderExistence();
        $ticketSeats = [];
        foreach ($seats as $seat) {
            if ($seat instanceof TicketSeat) {
                $seat->Destination = $this->order->Destination->Id;
                $ticketSeats[] = $seat;
            }
        }

        $response = $this->service->addTickets($this->order->Number, $seats);
        if ($response instanceof Order) {
            $this->order = $this->order->copy($response);
            $this->save();
            return true;
        }

        return false;
    }

    /**
     * @param array $numbers
     * @return bool
     * @throws HttpException
     * @throws \ReflectionException
     */
    public function deleteTickets(array $numbers)
    {
        $this->checkOrderExistence();
        $ticketSeats = [];
        foreach ($numbers as $number) {
            if (!empty($ticket = $this->order->getTicket($number))) {
                $seat = TicketSeat::createObject();
                $seat->SeatNum = $ticket->SeatNum;
                $seat->FareName = $ticket->FareName;
                $ticketSeats[] = $seat;
            }
        }

        if (!empty($ticketSeats)) {
            $response = $this->service->deleteTickets($this->order->Number, $ticketSeats);
            if ($response instanceof Order) {
                $this->order = $this->order->copy($response);
                $this->save();
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $ticketsData
     * @return bool
     * @throws HttpException
     * @throws \ReflectionException
     */
    public function setTicketsData(array $ticketsData)
    {
        $this->checkOrderExistence();
        $ticketsParam = &$this->order->Tickets;

        if (is_array($ticketsParam)) {
            foreach ($ticketsParam as &$ticket) {
                if (array_key_exists($ticket->Number, $ticketsData)) {
                    $ticket->setPersonalData($ticketsData[$ticket->Number]);
                    $ticket->setMetaData($ticketsData[$ticket->Number]);
                }
            }
        } elseif (($ticketsParam instanceof Ticket) && array_key_exists($ticketsParam->Number, $ticketsData)) {
            $ticketsParam->setPersonalData($ticketsData[$this->order->Tickets->Number]);
            $ticketsParam->setMetaData($ticketsData[$this->order->Tickets->Number]);
        } else {
            return false;
        }

        if (!$this->order->checkSeatNumbers()) {
            return false;
        }

        $response = $this->service->setTicketsData($this->order->Number, is_array($ticketsParam) ? $ticketsParam : [$ticketsParam]);
        if ($response instanceof Order) {
            $this->order = $this->order->copy($response, true);
//            TODO: Пока без проверки. Если в итоге проверка не понадобится - убрать весь закомментированный кусок
//            if (!$this->order->checkTicketsPersonalData()) {
//                throw new HttpException(400, 'Указаны не все данные пассажира');
//            }

            $this->save();
            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws HttpException
     */
    public function reserveOrder()
    {
        $this->checkOrderExistence();
        if ($this->createOrder()) {
            $response = $this->service->reserveOrder($this->order->Number);
            if ($response instanceof Order) {
                $this->order = $response;
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function getPrivileges(): array
    {
        $this->checkOrderExistence();
        $response = [];

        if (is_array($this->order->Tickets)) {
            foreach ($this->order->Tickets as $ticket) {
                ($ticket instanceof Ticket) && $response[$ticket->Number] = $this->service->getPrivileges($this->order->Number, $ticket->Number);
            }
        } elseif ($this->order->Tickets instanceof Ticket) {
            $response[$this->order->Tickets->Number] = $this->service->getPrivileges($this->order->Number, $this->order->Tickets->Number);
        }

        return $response;
    }

    /**
     * @param string $ticketNumber
     * @param string $privilegeName
     * @param bool $checkExistence
     * @return Ticket|null
     * @throws HttpException
     */
    public function setPrivilege(string $ticketNumber, string $privilegeName, $checkExistence = true): ?Ticket
    {
        $this->checkOrderExistence();

        if (!$checkExistence || $this->checkTicketExistence($ticketNumber)) {
            $response = $this->service->setPrivilege($this->order->Number, $ticketNumber, $privilegeName);

            if ($response instanceof Ticket) {
                if (is_array($this->order->Tickets)) {
                    foreach ($this->order->Tickets as $key => $ticket) {
                        $ticket->Number === $response->Number && $this->order->Tickets[$key] = $response;
                    }
                } elseif ($this->order->Tickets instanceof Ticket) {
                    $this->order->Tickets = $response;
                } else {
                    throw new HttpException(500, 'Ошибка сервера');
                }

                $this->save();
                return $response;
            }
        }

        return null;
    }

    /**
     * @param array $ticketsData
     * @return bool
     * @throws HttpException
     */
    public function setPrivilegeMultiple(array $ticketsData)
    {
        $this->checkOrderExistence();

        foreach ($ticketsData as $ticketNumber => $ticketData) {
            if (is_array($ticketData) && array_key_exists('privilege', $ticketData) &&  $this->checkTicketExistence($ticketNumber)) {
                if ($this->setPrivilege($ticketNumber, $ticketData['privilege'], false) === null) {
                    throw new HttpException('Не удалось установить льготы');
                }
            }
        }

        return true;
    }

    /**
     * @return array|null
     * @throws HttpException
     */
    public function getOccupiedSeats(): ?array
    {
        $this->checkOrderExistence();
        if (($this->order instanceof Order) &&
            ($this->order->Trip instanceof Trip) &&
            ($this->order->Departure instanceof BusStop) &&
            ($this->order->Destination instanceof BusStop)) {

            $result = $this->service->getOccupiedSeats($this->order->Trip->Id, $this->order->Departure->Id, $this->order->Destination->Id, $this->order->Number)['seats'];
            return ($result instanceof BusSeat) ? [$result] : $result;
        }
        return null;
    }

    /**
     * @return static
     */
    public static function getInstance(): self
    {
        if (static::$instance === null) {
            static::$instance = new self();
        }

        return static::$instance;
    }
}