<?php
namespace app\modules\gds\components\interfaces;

/**
 * Interface TicketReturnInterface
 * @package app\modules\gds\components\interfaces
 */
interface TicketReturnInterface
{
    /**
     * @return string
     */
    public function getTicketNumber(): string;

    /**
     * Формат возвращаемого значения GdsName_orderId
     * @return string
     */
    public function getOrderId(): string;

    /**
     * @return string
     */
    public function getUserId(): string;
    /**
     * @return string
     */
    public function getUserName(): string;

    /**
     * @return string
     */
    public function getCreated(): string;

    /**
     * @return string
     */
    public function getStatus(): string;

    /**
     * @return float
     */
    public function getAmount();

    /**
     * Возвращает массив экземпляров вызывающего класса. Ключами должны быть значения, возвращаемые функцией getIndex()
     * @param array $params
     * @return TicketReturnInterface[]
     */
    public static function search(array $params): array;
}