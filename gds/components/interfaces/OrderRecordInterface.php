<?php

namespace app\modules\gds\components\interfaces;

/**
 * Interface OrderRecordInterface
 * @package app\modules\gds\components\interfaces
 */
interface OrderRecordInterface
{

    /**
     * Метод должен возвращать строку, которая будет использоваться как ключ массива, возвращаемого функцией <OrderRecord>::search()
     * Формат возвращаемого значения GdsName_id
     * @return string
     */
    public function getIndex(): string;

    /**
     * @return int|string
     */
    public function getId();

    /**
     * @return string
     */
    public function getNumber();

    /**
     * @return string
     */
    public function getCreated();

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return string
     */
    public function getStatusLabel();

    /**
     * @return string
     */
    public function getRouteString();

    /**
     * @return string|null
     */
    public function getDepartureId(): ?string;

    /**
     * @return string
     */
    public function getDepartureString();

    /**
     * @return string
     */
    public function getDepartureAddress();

    /**
     * @return string|null
     */
    public function getDestinationId(): ?string;

    /**
     * @return string
     */
    public function getDestinationString();

    /**
     * @return string
     */
    public function getDestinationAddress();

    /**
     * @return string
     */
    public function getDepartureTime();

    /**
     * @return string
     */
    public function getDepartureTimeString();

    /**
     * @return string
     */
    public function getDestinationTimeString();

    /**
     * @return string
     */
    public function getBusString();

    /**
     * @return string
     */
    public function getCarrier();

    /**
     * @return array
     */
    public function getTickets(): array;

    /**
     * @return float
     */
    public function getCost():float;

    /**
     * @return string
     */
    public function getCostString(): string;

    /**
     * @return array
     */
    public function getOrderBundle():array;

    /**
     * @return int
     */
    public function getPlatform():int;

    /**
     * @return bool
     */
    public function isActual():bool;

    /**
     * Возвращает массив экземпляров вызывающего класса. Ключами должны быть значения, возвращаемые функцией getIndex()
     * @param array $params
     * @return mixed
     */
    public static function search(array $params): array;

    /**
     * Метод аналогичен ActiveRecord::findOne()
     * @param $id
     * @return static|null
     */
    public static function findRecord($id): ?OrderRecordInterface;
}